/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AntGame;

import Ant.*;
import Ant.Team;
import Ant.TeamBrain;
import World.World;
import World.WorldGenerator;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class StatisticsTest {
    Statistics stats;
    Ant ant, ant2;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    public StatisticsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        stats = new Statistics();
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = new World("world.World");
        tb = new TeamBrain(new HashMap<>());
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
        ant2 = new Ant(0, 2, 1, Team.RED, tb);
        
        ant.setDead(true);
        ant2.setFoodDelivered(13);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculate method, of class Statistics.
     */
    @Test
    public void testCalculate() {
        for (int i = 0; i < 150; i++) {
            for (int j = 0; j < 150; j++) {
                world.getGrid()[i][j].setFood(1);
            }
            
        }
        world.getGrid()[90][50].setFood(5);
          stats.calculate(new Ant[]{ant2}, new Ant[] {ant}, world);
        world.display();
        assertEquals(127,stats.getBlackTeamScore());
        assertEquals(127,stats.getRedTeamScore());
        assertTrue((double)127==stats.getRedAntAverageScore());
        assertTrue((double)127==stats.getBlackAntAverageScore());
        assertEquals(1,stats.getBlackAntsDead());
        assertEquals(0,stats.getBlackAntsAlive());
        assertEquals(0,stats.getRedAntsDead());
        assertEquals(1,stats.getRedAntsAlive());
    }

}