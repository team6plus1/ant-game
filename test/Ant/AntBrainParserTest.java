/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import World.*;

/**
 *
 * @author James
 */
public class AntBrainParserTest {

    private AntBrainParser validAntBrain;
    private AntBrainParser invalidAntBrain;
    private HashMap instructionSet;

    public AntBrainParserTest() {
    }

    @Before
    public void setUp() {
        try {
        WorldGenerator worldGen = new WorldGenerator(150, 2, 7, 11, 5,14, 1);
        World world = worldGen.getMap();
        world.outputFile("world2");
            validAntBrain = new AntBrainParser("davesUltimate.ant");
        } catch (brainException ex) {
            Logger.getLogger(AntBrainParserTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AntBrainParserTest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }

    /**
     * Test of getInstructions method, of class AntBrainParser.
     */
    @Test
    public void testGetInstructions() {
        assertNotNull(validAntBrain.getInstructions());
        assert(validAntBrain.getInstructions() instanceof HashMap);
        assert(validAntBrain.getInstructions().get(0) instanceof Flip);
        assert(validAntBrain.getInstructions().get(1) instanceof Flip);
        assert(validAntBrain.getInstructions().get(2) instanceof Flip);
        assert(validAntBrain.getInstructions().get(3) instanceof Turn);
        assert(validAntBrain.getInstructions().get(4) instanceof Turn);
        assert(validAntBrain.getInstructions().get(5) instanceof Turn);
        assert(validAntBrain.getInstructions().get(6) instanceof Turn);
        assert(validAntBrain.getInstructions().get(7) instanceof Move);
        assert(validAntBrain.getInstructions().get(8) instanceof Sense);
        assert(validAntBrain.getInstructions().get(9) instanceof PickUp);
        assert(validAntBrain.getInstructions().get(10) instanceof Sense);
        assert(validAntBrain.getInstructions().get(11) instanceof Drop);
        
    }

    /**
     * Test of getStatus method, of class AntBrainParser.
     */
    @Test
    public void testGetStatus() {
        try {
            validAntBrain = new AntBrainParser("sample.ant");
        } catch (brainException ex) {
            assertFalse(true);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AntBrainParserTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            invalidAntBrain = new AntBrainParser("broken.ant");
        } catch (brainException ex) {
            assertTrue(true);
        } catch (FileNotFoundException ex) {
            System.out.println("Ant brain not found");
        }
        assertTrue(validAntBrain.getStatus());
        assertNull(invalidAntBrain);
    }

}
