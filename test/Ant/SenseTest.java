/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import World.WorldGenerator;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class SenseTest {
    Ant ant, ant2;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    
    public SenseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new Sense(SenseType.HERE, 0, 1, InstructionType.FRIEND));
        instruction.put(1, new Sense(SenseType.AHEAD, 0, 2, InstructionType.FOE));
        instruction.put(2, new Sense(SenseType.RIGHTAHEAD, 0, 3, InstructionType.ROCK));
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
        ant2 = new Ant(0, 2, 1, Team.RED, tb);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class Sense.
     */
    @Test
    public void testExecute() {
        ant.makeMove();
        assertEquals(ant.getState(), 1);
        ant.makeMove();
        assertEquals(ant.getState(), 2);
        ant.makeMove();
        assertEquals(ant.getState(), 3);
    }
}