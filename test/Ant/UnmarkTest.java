/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import World.WorldGenerator;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class UnmarkTest {
    Ant ant;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    
    public UnmarkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new Mark(0, 1));
        instruction.put(1, new Mark(1, 2));
        instruction.put(2, new Mark(2, 3));
        instruction.put(3, new Mark(3, 4));
        instruction.put(4, new Mark(4, 5));
        instruction.put(5, new Mark(5, 6));
        instruction.put(6, new Unmark(0, 7));
        instruction.put(7, new Unmark(1, 8));
        instruction.put(8, new Unmark(2, 9));
        instruction.put(9, new Unmark(3, 10));
        instruction.put(10, new Unmark(4, 11));
        instruction.put(11, new Unmark(5, 0));
        
        
        
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class Unmark.
     */
    @Test
    public void testExecute() {
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        assertFalse(world.getGrid()[1][1].getMarkedBlack()[0]);
        assertFalse(world.getGrid()[1][1].getMarkedBlack()[1]);
        assertFalse(world.getGrid()[1][1].getMarkedBlack()[2]);
        assertFalse(world.getGrid()[1][1].getMarkedBlack()[3]);
        assertFalse(world.getGrid()[1][1].getMarkedBlack()[4]);
        assertFalse(world.getGrid()[1][1].getMarkedBlack()[5]);
    }
}