/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import World.WorldGenerator;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class TurnTest {
    Ant ant;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    
    public TurnTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new Turn(LRType.LEFT, 1));
        instruction.put(1, new Turn(LRType.RIGHT, 0));
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
    }
    
    @After
    public void tearDown() {
       
    }

    /**
     * Test of execute method, of class Turn.
     */
    @Test
    public void testExecute() {
        ant.makeMove();
        assertEquals(ant.getDirection(), 5);
        ant.makeMove();
        assertEquals(ant.getDirection(), 0);
    }
}