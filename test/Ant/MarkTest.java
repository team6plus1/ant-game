/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import World.WorldGenerator;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class MarkTest {
    Ant ant;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    
    public MarkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new Mark(0, 1));
        instruction.put(1, new Mark(1, 2));
        instruction.put(2, new Mark(2, 3));
        instruction.put(3, new Mark(3, 4));
        instruction.put(4, new Mark(4, 5));
        instruction.put(5, new Mark(5, 0));
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class Mark.
     */
    @Test
    public void testExecute() {
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        ant.makeMove();
        
        assertTrue(world.getGrid()[1][1].getMarkedBlack()[0]);
        assertTrue(world.getGrid()[1][1].getMarkedBlack()[1]);
        assertTrue(world.getGrid()[1][1].getMarkedBlack()[2]);
        assertTrue(world.getGrid()[1][1].getMarkedBlack()[3]);
        assertTrue(world.getGrid()[1][1].getMarkedBlack()[4]);
        assertTrue(world.getGrid()[1][1].getMarkedBlack()[5]);
        
    }
}