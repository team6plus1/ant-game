/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import World.WorldGenerator;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class PickUpTest {
    Ant ant;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    public PickUpTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new PickUp(0, 1));
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        world.getGrid()[1][1].setFood(1);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class PickUp.
     */
    @Test
    public void testExecute() {
        ant.makeMove();
        assertTrue(ant.HasFood());
        assertEquals(world.getGrid()[1][1].getFood(), 0);
    }
}