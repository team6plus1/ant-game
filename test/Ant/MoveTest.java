/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import World.WorldGenerator;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class MoveTest {
    Ant ant;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    
    public MoveTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new Move(1, 0));
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class Move.
     */
    @Test
    public void testExecute() {
        ant.makeMove();
        assertEquals(ant.getPosX(), 2);
        assertEquals(ant.getPosY(), 1);
        
    }
}