/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.*;
import Ant.*;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jw479
 */
public class DropTest {
    Ant ant;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    public DropTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new Drop(0));
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
        ant.setHasFood(true);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class Drop.
     */
    @Test
    public void testExecute() {
        ant.makeMove();
        assertFalse(ant.HasFood());
    }
}