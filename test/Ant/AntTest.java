/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ant;

import World.World;
import World.WorldGenerator;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author James
 */
public class AntTest {
    Ant ant, ant2;
    TeamBrain tb;
    WorldGenerator worldGenerator;
    World world;
    
    public AntTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
       worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        HashMap<Integer, Instruction> instruction = new HashMap<>();
        instruction.put(0, new Drop(1));
        tb = new TeamBrain(instruction);
        tb.addWorld(world);
        ant = new Ant(0, 1, 1, Team.BLACK, tb);
        ant.setHasFood(true);
        
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of makeMove method, of class Ant.
     */
    @Test
    public void testMakeMove() {
        ant.setDead(true);
        ant.makeMove();
        assertEquals(0, ant.getState());
        
        ant.setState(0);
        ant.setDead(false);
        ant.makeMove();
        assertEquals(1, ant.getState());
        
        ant.setState(0);
        ant.setResting(1);
        ant.makeMove();
        assertEquals(0, ant.getState());
    }
   
}
