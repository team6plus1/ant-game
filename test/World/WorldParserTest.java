/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author James
 */
public class WorldParserTest {

    WorldParser worldParser;

    public WorldParserTest() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testParser() {
        try {
            worldParser = new WorldParser("world.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (false);
        }
    }

    @Test
    public void testIncorrectDimentions() {
        try {
            worldParser = new WorldParser("incorrectDimWorld.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (true);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (false);
        }
    }

    @Test
    public void testIncorrectCharWorld() {
        try {
            worldParser = new WorldParser("incorrectCharWorld.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (true);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (false);
        }
    }

    @Test
    public void testAdjacentRockWorld() {
        try {
            worldParser = new WorldParser("adjacentRockWorld.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (true);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (false);
        }
    }

    @Test
    public void testIncorrectPerimeterWorld() {
        try {
            worldParser = new WorldParser("incorrectPerimeterWorld.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (true);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (false);
        }
    }

    @Test
    public void testIncorrectFoodShape() {
        try {
            worldParser = new WorldParser("incorrectFoodShapeWorld.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (true);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (false);
        }
    }

    @Test
    public void testIncorrectTournament() {
        try {
            worldParser = new WorldParser("incorrectTournamentWorld.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (true);
        } catch (AntHillException ex) {
            assert (false);
        }
    }

    @Test
    public void testIncorrectAnthill() {
        try {
            worldParser = new WorldParser("incorrectAnthillWorld.World");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (true);
        }

    }

    /**
     * Test of getStatus method, of class WorldParser.
     */
    @Test
    public void testGetStatus() {
        try {
            worldParser = new WorldParser("world.world");
        } catch (FileNotFoundException ex) {
            assert (false);
        } catch (DimensionException ex) {
            assert (false);
        } catch (InvalidTypeException ex) {
            assert (false);
        } catch (PathException ex) {
            assert (false);
        } catch (PerimeterException ex) {
            assert (false);
        } catch (FoodException ex) {
            assert (false);
        } catch (TournmentException ex) {
            assert (false);
        } catch (AntHillException ex) {
            assert (false);
        }
        assert (worldParser.getStatus());
    }

}
