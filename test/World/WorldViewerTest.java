/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author James
 */
public class WorldViewerTest {

    WorldGenerator worldGenerator;
    World world;
    WorldViewer worldViewer;
    Hexagon[][] hexGrid;
    int[] hexPoints;

    public WorldViewerTest() {
    }

    @Before
    public void setUp() {
        worldGenerator = new WorldGenerator(150, 2, 7, 11, 5, 14, 1);
        world = worldGenerator.getMap();
        //worldViewer = new WorldViewer(world);
    }

    /**
     * Test of hexPointsCreator method, of class WorldViewer.
     */
    @Test
    public void testHexPointsCreator() {
        int[] expected = new int[]{14, 14, 11, 12, 11, 8, 14, 6, 18, 8, 18, 12};
        hexPoints = new WorldViewer(world).hexPointsCreator(1, 1);
        assertArrayEquals(expected, hexPoints);
    }

    /**
     * Test of createHexagons method, of class WorldViewer.
     */
    @Test
    public void testCreateHexagons() {
        worldViewer = new WorldViewer(world);
        worldViewer.createHexagons();
        hexGrid = worldViewer.getHexGrid();
        assertEquals(150, hexGrid[0].length);
        assertEquals(150, hexGrid.length);

    }

}
