/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Polygon;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author James
 */
public class HexagonTest {

    private Polygon polygons;
    private Hexagon hexs;
    private int[] xPoints;
    private int[] yPoints;
    private int[] hexPoints;

    public HexagonTest() {
    }

    @Before
    public void setUp() {
        xPoints = new int[]{15, 13, 8, 5, 7, 12};
        yPoints = new int[]{10, 6, 6, 10, 14, 14};
        hexPoints = new int[]{15, 10, 13, 6, 8, 6, 5, 10, 7, 14, 12, 14};
        hexs = new Hexagon(hexPoints);

    }

    /**
     * Test of getPolygon method, of class Hexagon.
     */
    @Test
    public void testGetPolygon() {
        assertArrayEquals(xPoints, hexs.getPolygon().xpoints);
        assertArrayEquals(yPoints, hexs.getPolygon().ypoints);
    }

}
