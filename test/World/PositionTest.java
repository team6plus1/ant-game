/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author James
 */
public class PositionTest {
    private Position position;
    private int[] upLeft;
    private int[] upRight;
    private int[] downLeft;
    private int[] downRight;
    private int[] right;
    private int[] left;
    public PositionTest() {
    }
    
    @Before
    public void setUp() {
        position = new Position(5, 5);
        upLeft = new int[]{5,4};
        upRight = new int[]{6,4};
        downLeft = new int[]{5,6};
        downRight = new int[]{6,6};
        right = new int[]{6,5};
        left = new int[]{4,5};
        
    }

    /**
     * Test of upLeft method, of class Position.
     */
    @Test
    public void testUpLeft() {
        
        assertArrayEquals(upLeft, Position.upLeft(5, 5));
    }

    /**
     * Test of upRight method, of class Position.
     */
    @Test
    public void testUpRight() {
        assertArrayEquals(upRight, Position.upRight(5,5));
    }

    /**
     * Test of downLeft method, of class Position.
     */
    @Test
    public void testDownLeft() {
       assertArrayEquals(downLeft, Position.downLeft(5,5));
    }

    /**
     * Test of downRight method, of class Position.
     */
    @Test
    public void testDownRight() {
        assertArrayEquals(downRight, Position.downRight(5,5));
    }

    /**
     * Test of right method, of class Position.
     */
    @Test
    public void testRight() {
        assertArrayEquals(right, Position.right(5,5));
    }

    /**
     * Test of left method, of class Position.
     */
    @Test
    public void testLeft() {
       assertArrayEquals(left, Position.left(5,5));
    }

    /**
     * Test of getUpLeft method, of class Position.
     */
    @Test
    public void testGetUpLeft() {
       assertArrayEquals(upLeft, position.getUpLeft());
    }

    /**
     * Test of getUpRight method, of class Position.
     */
    @Test
    public void testGetUpRight() {
        assertArrayEquals(upRight, position.getUpRight());
    }

    /**
     * Test of getDownLeft method, of class Position.
     */
    @Test
    public void testGetDownLeft() {
        assertArrayEquals(downLeft, position.getDownLeft());
    }

    /**
     * Test of getDownRight method, of class Position.
     */
    @Test
    public void testGetDownRight() {
        assertArrayEquals(downRight, position.getDownRight());
    }

    /**
     * Test of getRight method, of class Position.
     */
    @Test
    public void testGetRight() {
        assertArrayEquals(right, position.getRight());
    }

    /**
     * Test of getLeft method, of class Position.
     */
    @Test
    public void testGetLeft() {
        assertArrayEquals(left, position.getLeft());
    }
    
}
