/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import Ant.Ant;
import Ant.AntBrainParser;
import Ant.Team;
import Ant.TeamBrain;
import Ant.brainException;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author James
 */
public class CellTest {

    private Cell[] cells;

    public CellTest() {
    }

    @Before
    public void setUp() {
        cells = new Cell[20];

        cells[0] = new Cell(CellType.BLACKANTHILL);
        CellType temp = cells[0].getType();
        temp.name();
        cells[1] = new Cell(CellType.REDANTHILL);
        cells[2] = new Cell(CellType.CLEAR);
        cells[3] = new Cell(CellType.FOOD);
        cells[4] = new Cell(CellType.ROCK);

    }

    /**
     * Test of getType method, of class Cell.
     */
    @Test
    public void testGetType() {
        assert (cells[0].getType().name().equals("BLACKANTHILL"));
        assert (cells[1].getType().name().equals("REDANTHILL"));
        assert (cells[2].getType().name().equals("CLEAR"));
        assert (cells[3].getType().name().equals("FOOD"));
        assert (cells[4].getType().name().equals("ROCK"));

    }

    /**
     * Test of getChecked method, of class Cell.
     */
    @Test
    public void testGetChecked() {
        assertFalse(cells[0].getChecked());
        cells[0].setChecked();
        assertTrue(cells[0].getChecked());
    }

    /**
     * Test of setFood method, of class Cell.
     */
    @Test
    public void testSetFood() {
        assert (cells[2].getType().name().equals("CLEAR"));
        assertEquals(0, cells[2].getFood());
        int numFood = 2;
        cells[2].setFood(numFood);
        assert(cells[2].getType().equals(CellType.FOOD));
        assertEquals(numFood, cells[2].getFood());

    }

    /**
     * Test of decrementFood method, of class Cell.
     */
    @Test
    public void testDecrementFood() {
        assert (cells[2].getType().name().equals("CLEAR"));
        assertEquals(0, cells[2].getFood());
        int numFood = 2;
        cells[2].setFood(numFood);
        assert(cells[2].getType().equals(CellType.FOOD));
        assertEquals(numFood, cells[2].getFood());
        cells[2].decrementFood();
        assertEquals(numFood-1, cells[2].getFood());
    }

    /**
     * Test of setChecked method, of class Cell.
     */
    @Test
    public void testSetChecked() {
        assertFalse(cells[0].getChecked());
        cells[0].setChecked();
        assertTrue(cells[0].getChecked());
        assertFalse(cells[1].getChecked());
        cells[1].setChecked();
        assertTrue(cells[1].getChecked());
    }

    /**
     * Test of setRedAnt method, of class Cell.
     */
    @Test
    public void testSetRedAnt() {
        assert (!cells[2].containsRedAnt());
        cells[2].setRedAnt(true);
        assert(cells[2].containsRedAnt());
    }

    /**
     * Test of setBalckAnt method, of class Cell.
     */
    @Test
    public void testSetBlackAnt() {
        assert (!cells[2].containsBlackAnt());
        cells[2].setBlackAnt(true);
        assert(cells[2].containsBlackAnt());
    }

    /**
     * Test of setAnt method, of class Cell.
     */
    @Test
    public void testSetAnt() throws brainException {
        try {
            WorldGenerator worldGenerator = new WorldGenerator(150, 2,20 , 11, 15, 14, 1);
            World world = worldGenerator.getMap();
            AntBrainParser t1 = new AntBrainParser("elly.ant");
            TeamBrain tb1 = new TeamBrain(t1.getInstructions());
            AntBrainParser t2 = new AntBrainParser("davesUltimate.ant");
            TeamBrain tb2 = new TeamBrain(t2.getInstructions());
            tb1.addWorld(world);
            tb2.addWorld(world);
            Ant ant = new Ant(4, 2, 3, Team.BLACK, tb2);
            
            cells[2].setAnt(ant);
            assert(cells[2].getAnt().equals(ant));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CellTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Test of containsRedAnt method, of class Cell.
     */
    @Test
    public void testContainsRedAnt() {
        assert (!cells[2].containsRedAnt());
        cells[2].setRedAnt(true);
        assert(cells[2].containsRedAnt());
    }

    /**
     * Test of containsBlackAnt method, of class Cell.
     */
    @Test
    public void testContainsBlackAnt() {
       assert (!cells[2].containsBlackAnt());
        cells[2].setBlackAnt(true);
        assert(cells[2].containsBlackAnt());
    }

    /**
     * Test of setMarkedBlack method, of class Cell.
     */
    @Test
    public void testSetMarkedBlack() {
        assert(!cells[2].getMarkedBlack()[0]);
        cells[2].setMarkedBlack(0, true);
        assert(cells[2].getMarkedBlack()[0]);
    }

    /**
     * Test of setMarkedRed method, of class Cell.
     */
    @Test
    public void testSetMarkedRed() {
        assert(!cells[2].getMarkedRed()[0]);
        cells[2].setMarkedRed(0, true);
        assert(cells[2].getMarkedRed()[0]);
    }

    /**
     * Test of getAnt method, of class Cell.
     */
    @Test
    public void testGetAnt() throws brainException {
        try {
            WorldGenerator worldGenerator = new WorldGenerator(150, 2,20 , 11, 15, 14, 1);
            World world = worldGenerator.getMap();
            AntBrainParser t1 = new AntBrainParser("elly.ant");
            TeamBrain tb1 = new TeamBrain(t1.getInstructions());
            AntBrainParser t2 = new AntBrainParser("davesUltimate.ant");
            TeamBrain tb2 = new TeamBrain(t2.getInstructions());
            tb1.addWorld(world);
            tb2.addWorld(world);
            Ant ant = new Ant(4, 2, 3, Team.BLACK, tb2);
            
            cells[2].setAnt(ant);
            assert(cells[2].getAnt().equals(ant));;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CellTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Test of getMarkedBlack method, of class Cell.
     */
    @Test
    public void testGetMarkedBlack() {
        assert(!cells[2].getMarkedBlack()[0]);
        cells[2].setMarkedBlack(0, true);
        assert(cells[2].getMarkedBlack()[0]);
    }

    /**
     * Test of getMarkedRed method, of class Cell.
     */
    @Test
    public void testGetMarkedRed() {
        assert(!cells[2].getMarkedRed()[0]);
        cells[2].setMarkedRed(0, true);
        assert(cells[2].getMarkedRed()[0]);
    }

    /**
     * Test of previousType method, of class Cell.
     */
    @Test
    public void testPreviousType() {
        int numFood = 3;
        assert (cells[0].getType().name().equals("BLACKANTHILL"));
        assert (cells[1].getType().name().equals("REDANTHILL"));
        assert (cells[2].getType().name().equals("CLEAR"));
        assert (cells[3].getType().name().equals("FOOD"));
        assert (cells[4].getType().name().equals("ROCK"));
        cells[0].setFood(numFood);
        cells[1].setFood(numFood);
        cells[2].setFood(numFood);
        cells[3].setFood(numFood);
        cells[4].setFood(numFood);
        assert (cells[0].getType().name().equals("FOOD"));
        assert (cells[0].getType().name().equals("FOOD"));
        assert (cells[0].getType().name().equals("FOOD"));
        assert (cells[0].getType().name().equals("FOOD"));
        assert (cells[0].getType().name().equals("FOOD"));
        assert (cells[0].previousType().name().equals("BLACKANTHILL"));
        assert (cells[1].previousType().name().equals("REDANTHILL"));
        assert (cells[2].previousType().name().equals("CLEAR"));
        assert (cells[3].previousType().name().equals("FOOD"));
        assert (cells[4].previousType().name().equals("ROCK"));
        
        
        
    }

    /**
     * Test of getFood method, of class Cell.
     */
    @Test
    public void testGetFood() {
        assert (cells[2].getType().name().equals("CLEAR"));
        assertEquals(0, cells[2].getFood());
        int numFood = 2;
        cells[2].setFood(numFood);
        assert(cells[2].getType().equals(CellType.FOOD));
        assertEquals(numFood, cells[2].getFood());
    }

}
