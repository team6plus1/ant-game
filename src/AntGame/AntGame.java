package AntGame;

/**
 * Runs the ant game. Launches GUI.
 * @author Joel
 */
public class AntGame {
    public static void main(String [] args){
        new GUI.Menu().setVisible(true);
    }
}
