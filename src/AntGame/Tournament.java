/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AntGame;

import Ant.AntBrainParser;
import Ant.TeamBrain;
import Ant.Team;
import World.*;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Tournament class runs a tournament with the given teams and world.
 * @author Joel
 */
public class Tournament {
    /**
     * HashMap containing team names and team brains.
     */
    private HashMap<String, TeamBrain> teams;

    /**
     * HashMap containing team name and tournament points
     */
    private HashMap<String, Integer> leaderboard;

    /**
     * Creates an instance of Tournament
     */
    public Tournament() {
        teams = new HashMap<>();
    }

    /**
     * Adds a team's name and their brain to the tournament if name isn't already used.
     * @param teamName name of the team to be added.
     * @param teamBrain brain of the team to be added.
     * @throws TournamentException
     */
    public void addTeam(String teamName, TeamBrain teamBrain) throws TournamentException {
        if (!teams.containsKey(teamName)) {
            teams.put(teamName, teamBrain);
        } else {
            throw new TournamentException("Team name in use");
        }
    }

    /**
     * Generates the world for the tournament.
     * @return the generated world.
     * @throws TournamentException
     */
    public World genWorld() throws TournamentException {
        World w = new WorldGenerator(150, 2, 7, 11, 5, 14, 1).getMap();
        w.outputFile("world");
        try {
            WorldParser p = new WorldParser("world.World");
            if (p.getStatus()) {
                return w;
            } else {
                return genWorld();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new TournamentException("Parser Failed");
    }

    /**
     * Pairs the teams for battle and runs the tournament until completion.
     */
    public void runTournament() {       //algorithm for pairing submissions for battle
        leaderboard = new HashMap<>();
        for (String team : teams.keySet()) {
            leaderboard.put(team, 0);
        }
        runBattles();
        while(!checkWinner()){
            runBattles();
        }
    }

    /**
     * Runs all the battles for every team.
     */
    public void runBattles() {
        World world;
        try {
            world = genWorld();
        } catch (TournamentException e) {
            e.printStackTrace();
        }
        for (String redTeam : teams.keySet()) {
            for (String blackTeam : teams.keySet()) {
                if (!redTeam.equals(blackTeam)) {
                    world = new World("world.World");
                    teams.get(redTeam).addWorld(world);
                    teams.get(blackTeam).addWorld(world);
                    Battle b = new Battle(world, 300000, teams.get(redTeam), teams.get(blackTeam), true);
                    Team[] winner = b.getWinner();
                    addScore(winner, redTeam, blackTeam);

                    Statistics stats = b.getStatistics();
                    String s;
                    DecimalFormat df = new DecimalFormat("#.00");
                    if (stats.getWinner().length > 1) {
                        s = "DRAW!\n\n";
                    } else {
                        if (stats.getWinner()[0].name().equals("BLACK")) {
                            s = "WINNER: " + blackTeam + "\n\n";
                        } else {
                            s = "WINNER: " + redTeam + "\n\n";
                        }
                    }
                    s +=  redTeam + " team score: " + stats.getRedTeamScore() + "\n";
                    s += blackTeam + " team score: " + stats.getBlackTeamScore() + "\n\n";
                    s += redTeam + " ants dead: " + stats.getRedAntsDead() + "/" + (stats.getRedAntsDead() + stats.getRedAntsAlive()) + "\n";
                    s += redTeam + " ants high score: " + stats.getRedAntHighScore() + "\n";
                    s += redTeam + " ants average score: " + df.format(stats.getRedAntAverageScore()) + "\n\n";
                    s += blackTeam + " ants dead: " + stats.getBlackAntsDead() + "/" + (stats.getBlackAntsDead() +stats.getBlackAntsAlive()) + "\n";
                    s += blackTeam + " ant high score: " + stats.getBlackAntHighScore() + "\n";
                    s += blackTeam + " ant average score: " + df.format(stats.getBlackAntAverageScore());

                    JOptionPane.showMessageDialog(null, s, "AntGame: Battle result", JOptionPane.PLAIN_MESSAGE);
                }
            }
        }
    }

    /**
     * Returns true if the leaderboard has a winner.
     * @return a boolean.
     */
    public Boolean checkWinner(){
        int highScore = 0;
        int tie = 0;
        for(String team : leaderboard.keySet()){
            if(highScore < leaderboard.get(team)) {
                highScore = leaderboard.get(team);
            }
        }
        for(String team : leaderboard.keySet()){
            if(leaderboard.get(team) == highScore) {
                tie++;
            }
        }
        return tie <= 1;

    }

    /**
     * Gives the score to the teams after a battle.
     * @param winner used to get the winner.
     * @param redTeam used to get the red team.
     * @param blackTeam used to get the black team.
     */
    public void addScore(Team[] winner, String redTeam, String blackTeam) {
        if (winner.length == 1) {
            if (winner[0].equals(Team.BLACK)) {
                int score = leaderboard.get(blackTeam);
                leaderboard.put(blackTeam, score + 2);
            } else {
                int score = leaderboard.get(redTeam);
                leaderboard.put(redTeam, score + 2);
            }
        } else {
            for (Team team : winner) {
                if (team.equals(Team.BLACK)) {
                    int score = leaderboard.get(blackTeam);
                    leaderboard.put(blackTeam, score + 1);
                } else {
                    int score = leaderboard.get(redTeam);
                    leaderboard.put(redTeam, score + 1);
                }
            }
        }
    }

    /**
     * Getter for the leaderboard.
     * @return Returns the leaderboard.
     */
    public HashMap<String, Integer> getLeaderboard() {
        return leaderboard;
    }
}

/**
 * Creates the exception for the tournament.
 */
class TournamentException extends Exception {
    /**
     * Constructor for exception with message.
     * @param message error message.
     */
    public TournamentException(String message) { super(message); }
}
