package AntGame;

import Ant.Ant;
import Ant.Team;
import World.*;

/**
 * Collects statistics from the Battle.
 * @author David
 */
public class Statistics {

    /**
     * Red team's score.
     */
    private int redTeamScore;
    /**
     * Black team's score.
     */
    private int blackTeamScore;
    /**
     * Average red ant score.
     */
    private double redAntAverageScore;
    /**
     * Average black ant score.
     */
    private double blackAntAverageScore;
    /**
     * Best red ant score.
     */
    private int redAntHighScore;
    /**
     * Best black ant score.
     */
    private int blackAntHighScore;
    /**
     * Number of black ants alive.
     */
    private int blackAntsAlive;
    /**
     * Number of red ants alive.
     */
    private int redAntsAlive;
    /**
     * Number of red ants dead.
     */
    private int redAntsDead;
    /**
     * Number of black ants dead.
     */
    private int blackAntsDead;
    /**
     * Winner array. First is winner.
     */
    private Team[] winner;

    /**
     * Constructor for statistics. Initialises variables.
     */
    public Statistics() {
        redTeamScore = 0;
        blackTeamScore = 0;
        redAntAverageScore = 0;
        redAntHighScore = 0;
        blackAntHighScore = 0;
        redAntsDead = 0;
        blackAntsDead = 0;
    }

    /**
     * Gets all the statistics.
     * @param redAnts gets red ants.
     * @param blackAnts gets black ants.
     * @param world gets the world.
     */
    public void calculate(Ant[] redAnts, Ant[] blackAnts, World world) {
        Cell[][] grid = world.getGrid();
        calcFoodCollected(grid);
        blackAntsAlive = blackAnts.length;
        redAntsAlive = redAnts.length;
        for (Ant ant : redAnts) {
            if (ant.getFoodDelivered() >= redAntHighScore) {
                redAntHighScore = ant.getFoodDelivered();
            }
            if (ant.isDead()) {
                redAntsAlive--;
                redAntsDead++;
            }
        }

        for (Ant ant : blackAnts) {
            if (ant.getFoodDelivered() >= blackAntHighScore) {
                blackAntHighScore = ant.getFoodDelivered();
            }
            if (ant.isDead()) {
                blackAntsAlive--;
                blackAntsDead++;
            }
        }

        System.out.println("red Team Score: "+ redTeamScore);
        System.out.println("black Team Score: "+ blackTeamScore);
        redAntAverageScore = (double)redTeamScore / (double)redAnts.length;
        blackAntAverageScore = (double)blackTeamScore / (double)blackAnts.length;

        if (redTeamScore> blackTeamScore){
            winner = new Team[]{ Team.RED};
        } else if (blackTeamScore>redTeamScore){
            winner = new Team[]{ Team.BLACK};
        } else  winner = new Team[]{ Team.BLACK, Team.RED};
        displayStatistics();
    }


    /**
     * Gets food collected for both teams.
     * @param grid world.
     */
    private void calcFoodCollected(Cell[][] grid){
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[j][i].previousType()==CellType.REDANTHILL && grid[j][i].getType()==CellType.FOOD){
                    redTeamScore+= grid[j][i].getFood();
                }
                if (grid[j][i].previousType()==CellType.BLACKANTHILL && grid[j][i].getType()==CellType.FOOD){
                    blackTeamScore+= grid[j][i].getFood();
                }
            }
        }
    }

    /**
     * @return red team score.
     */
    public int getRedTeamScore() {
        return redTeamScore;
    }

    /**
     * @return black team score.
     */
    public int getBlackTeamScore() {
        return blackTeamScore;
    }

    /**
     * @return red ant average score.
     */
    public double getRedAntAverageScore() {
        return redAntAverageScore;
    }

    /**
     * @return black ant average score.
     */
    public double getBlackAntAverageScore() {
        return blackAntAverageScore;
    }

    /**
     * @return black ant high score.
     */
    public int getBlackAntHighScore() { return blackAntHighScore; }

    /**
     * @return red ant high score.
     */
    public int getRedAntHighScore() { return redAntHighScore; }

    /**
     * @return black ants alive.
     */
    public int getBlackAntsAlive() {
        return blackAntsAlive;
    }

    /**
     * @return red ants alive.
     */
    public int getRedAntsAlive() {
        return redAntsAlive;
    }

    /**
     * @return red ants dead.
     */
    public int getRedAntsDead() {
        return redAntsDead;
    }

    /**
     * @return black ants dead.
     */
    public int getBlackAntsDead() {
        return blackAntsDead;
    }

    /**
     * @return winner.
     */
    public Team[] getWinner() {
        return winner;
    }

    /**
     * Prints statistics to console.
     */
    public void displayStatistics() {
        if (winner.length > 1) {
            System.out.println("DRAW\n\n");
        } else
            System.out.println("Winner: " + winner[0].toString());
        System.out.println("Black Team Score: " + blackTeamScore);
        System.out.println("Red Team Score: " + redTeamScore);
        System.out.println("Red Ant Average Score: "+ redAntAverageScore);
        System.out.println("Black Ant Average Score: "+ blackAntAverageScore);
        System.out.println("Red Ant High Score: "+ redAntHighScore);
        System.out.println("Black Ant High Score: "+ blackAntHighScore);
        System.out.println("Red Ants Alive: "+ redAntsAlive);
        System.out.println("Red Ants Dead: "+ redAntsDead);
        System.out.println("Black Ants Alive: "+ blackAntsAlive);
        System.out.println("Black Ants Dead: "+ blackAntsDead);
    }
}
