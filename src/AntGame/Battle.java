package AntGame;

import World.Cell;
import World.World;
import Ant.*;
import World.Position;
import World.WorldViewer;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * contains all methods and fields associated with a single battle between two ant brains on a single map.
 * Created by David on 09/04/2015.
 */
public class Battle {


    private final int speed =70; //speed for the game. max is 42857

    private int turnLimit;
    private int turnNum;
    private World world;
    private Cell[][] grid;
    private Ant[] redAnt;
    private Ant[] blackAnt;
    private Statistics statistics;
    private TeamBrain redBrain;
    private TeamBrain blackBrain;
    private WorldViewer worldViewer;
    boolean tournament = false;

    int redTeamFood;
    int blackTeamFood;

    /**
     * The battle constructor stores all the paramters in appropriate class field. then calls the intialise
     * ants method to spawn ent objects in each anthill
     * creates an instance of the statistics class to store and calculate battle information
     * creates an instance of the worldviewer to display the battle.
     * calls the run battle method
     * updates the stats window in world viewer.
     * @param world
     * @param turnLimit
     * @param redBrain
     * @param blackBrain
     */
    public Battle(World world, int turnLimit, TeamBrain redBrain, TeamBrain blackBrain)
    {

        this.turnLimit = turnLimit;
        this.world = world;
        grid = world.getGrid();


        this.redBrain = redBrain;
        this.blackBrain = blackBrain;
        blackAnt = new Ant[world.getNumBlackAnts()];
        redAnt = new Ant[world.getNumRedAnts()];


        turnNum = 1;
        statistics = new Statistics();

        intialiseAnts();
        worldViewer = new WorldViewer(world);
        runBattle();

        Statistics stats = this.getStatistics();
        String s;
        DecimalFormat df = new DecimalFormat("#.00");
        if (stats.getWinner().length > 1) {
            s = "DRAW!\n\n";
        } else {
            s = "WINNER: " + stats.getWinner()[0].toString() + "\n\n";
        }
        s += "Red team score: " + stats.getRedTeamScore() + "\n";
        s += "Black team score: " + stats.getBlackTeamScore() + "\n\n";
        s += "Red ants dead: " + stats.getRedAntsDead() + "/" + (stats.getRedAntsDead() + stats.getRedAntsAlive()) + "\n";
        s += "Red ant high score: " + stats.getRedAntHighScore() + "\n";
        s += "Red ant average score: " + df.format(stats.getRedAntAverageScore()) + "\n\n";
        s += "Black ants dead: " + stats.getBlackAntsDead() + "/" + (stats.getBlackAntsDead() +stats.getBlackAntsAlive()) + "\n";
        s += "Black ant high score: " + stats.getBlackAntHighScore() + "\n";
        s += "Black ant average score: " + df.format(stats.getBlackAntAverageScore());

        JOptionPane.showMessageDialog(null, s, "AntGame: Battle result", JOptionPane.PLAIN_MESSAGE);
        worldViewer.closeWindow();
    }


    /**
     * * This battle constructor stores all the paramters in appropriate class fields. then calls the intialise
     * ants method to spawn ent objects in each anthill
     * creates an instance of the statistics class to store and calculate battle information
     *
     * calls the run battle method
     *
     * @param world
     * @param turnLimit
     * @param redBrain
     * @param blackBrain
     * @param isTournament
     */
    public Battle(World world, int turnLimit, TeamBrain redBrain, TeamBrain blackBrain, boolean isTournament)
    {

        this.turnLimit = turnLimit;
        this.world = world;
        grid = world.getGrid();
        this.tournament = isTournament;

        this.redBrain = redBrain;
        this.blackBrain = blackBrain;
        blackAnt = new Ant[world.getNumBlackAnts()];
        redAnt = new Ant[world.getNumRedAnts()];


        turnNum = 1;
        statistics = new Statistics();

        intialiseAnts();
        runBattle();


    }

    /**
     * creates an ant object for every cell that is of type anthill.
     * redAnts on the red anthill. black ants on the black anthill
     */
    public void intialiseAnts() {
        int redAntCount = 0;
        int blackAntCount = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                if (grid[j][i].containsBlackAnt()) {
                    blackAnt[blackAntCount] = new Ant(blackAntCount, j, i, Team.BLACK, blackBrain);
                    blackAntCount++;
                } else if (grid[j][i].containsRedAnt()) {
                    redAnt[redAntCount] = new Ant(redAntCount, j, i, Team.RED, redBrain);
                    redAntCount++;
                }
            }
        }
        System.out.println(redAntCount);
        System.out.println(blackAntCount);
    }

    /**
     * calls the next turn method for amount of turns specified by turnLimit
     * calculates statistics for battle
     * returns the winning team
     * @return  Team[] array storing the winning team or both teams if the battle is a draw
     */
    public Team[] runBattle() {
        while (battleNotOver()) {
            nextTurn();
        }
        statistics.calculate(redAnt, blackAnt, world);
        return getWinner();
    }

    /**
     * calls the makeMove effort for every ant on the map.
     * calls the find dying ants method which will remove any ants surrounded by enemies
     * if game is not a tournament then the worldviewer is updated.
     * tournament battles are not displayed visually
     */
    public void nextTurn() {
        ///perform turn shit here
        redTeamFood = 0;
        blackTeamFood = 0;
       // System.out.println(turnNum);

        for (Ant ant : redAnt) {
            ant.makeMove();
            redTeamFood += ant.getFoodDelivered();
        }

        for (Ant ant : blackAnt) {
            ant.makeMove();
            blackTeamFood += ant.getFoodDelivered();
        }

        findDyingAnts();
        turnNum++;
        //redraw World

        if (!tournament) {
            if (speed == 0) {
                worldViewer.updateWorld();
            } else if (turnNum % (7 * speed) == 0) {
                worldViewer.updateWorld();
            }
        }
    }

    /**
     * check to see if a battle is over
     * @return Boolean
     */
    public boolean battleNotOver() {

        return ((turnNum < turnLimit) /*&&  (world.getNumRedAnts()>0)  && (world.getNumBlackAnts()>0)*/);    //commented out code to end battle when one teams ants are all dead.

    }


    /**
     * calculates winning teams based on food collected by each team.
     * returns an array containing the winning team (teams if the battle is a draw)
     * @return Team[] showing the winner(s) of the battle if two teams are returned then the battle is a tie.
     */
    public Team[] getWinner() {
        if (statistics.getRedTeamScore() > statistics.getBlackTeamScore()) {
            return new Team[]{Team.RED};
        } else if ((statistics.getBlackTeamScore() > statistics.getRedTeamScore())) {
            return new Team[]{Team.BLACK};
        } else {
            return new Team[]{Team.RED, Team.BLACK};
        }

    }

    /**
     * returns position of adjacent cell in a set direction
     * @param i direction to look in
     * @param ant ant which is checking the adjacent cell
     * @return int[] containing position data
     */
    public int[] adjacentCell(int i, Ant ant) {
        Position position = new Position(ant.getPosX(), ant.getPosY());

        int[] p = null;
        switch (i) {
            case 0: //right
                p = position.getRight();
                break;
            case 1: //down right
                p = position.getDownRight();
                break;
            case 2: //down left
                p = position.getDownLeft();
                break;
            case 3: //left
                p = position.getLeft();
                break;
            case 4: //up left
                p = position.getUpLeft();
                break;
            case 5: //up right
                p = position.getUpRight();
                break;
        }
        return p;
    }

    /**
     *
     * checks to see if an ant is surrounded by looking at adjacent cells
     * @param ant
     * @return boolean
     */
    public boolean isSurrounded(Ant ant) {
        int surroundingAnts = 0;
        int notEnemyAnt = 0;
        int cellNum = 0;

        while (notEnemyAnt < 2 && cellNum < 6) {
            int[] adjacentPosition = adjacentCell(cellNum, ant);
            int x = adjacentPosition[0];
            int y = adjacentPosition[1];

            if (ant.getTeam() == Team.RED) {
                if (grid[x][y].containsBlackAnt()) {
                    //System.out.println("x: " + x + "y: " + y);
                    surroundingAnts++;
                } else {
                    notEnemyAnt++;
                }
            }

            if (ant.getTeam() == Team.BLACK) {
                if (grid[x][y].containsRedAnt()) {
                    surroundingAnts++;
                } else {
                    notEnemyAnt++;
                }
            }
            cellNum++;
        }


        if (surroundingAnts >= 5) {
            return true;
        } else return false;
    }

    /**
     * finds dying ants (ants surrounded by enemies) and adds trhem to an arrayList of dying ants.
     * calls method killDyingAnts(dyingAnts) to kill these ants
     */
    public void findDyingAnts() {
        ArrayList<Ant> dyingAnts = new ArrayList<>();

        for (Ant ant : redAnt) {
        if(!ant.isDead()){
            if (isSurrounded(ant)) {
                dyingAnts.add(ant);
            }


        }}

        for (Ant ant : blackAnt) {
            if(!ant.isDead()){
            if (isSurrounded(ant)) {
                dyingAnts.add(ant);
            }}
        }


        killDyingAnts(dyingAnts);
    }

    /**
     * sets all ants that are dying to dead
     *
     * @param ants ants to be killed
     */
    public void killDyingAnts(ArrayList<Ant> ants) {
        for (Ant ant : ants) {
            ant.setDead(true);
            ant.setHasFood(false);
            grid[ant.getPosX()][ant.getPosY()].setFood(3);
            grid[ant.getPosX()][ant.getPosY()].setBlackAnt(false);
            grid[ant.getPosX()][ant.getPosY()].setRedAnt(false);
            grid[ant.getPosX()][ant.getPosY()].setAnt(null);


        }
    }

    /**
     * returns the statistics for battle
     * @return Statistics instance containing battle data
     */
    public Statistics getStatistics(){
        return  statistics;
    }

}
