package World;

/**
 * Gets a specific cell relative to the current one.
 * @author David
 */
public class Position {
    /**
     * Position x.
     */
    int x;
    /**
     * Position y.
     */
    int y;

    /**
     * Constructor that initialises the coordinates.
     * @param x coord x.
     * @param y coord y.
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets up-left cell.
     * @param x
     * @param y
     * @return coordinates.
     */
    public static int[] upLeft(int x, int y) {
        if (y % 2 == 0) {
            x--;
        } // adjust for indent on odd lines

        return new int[]{x, y - 1};
    }

    /**
     * Gets up-right cell.
     * @param x
     * @param y
     * @return coordinates.
     */
    public static int[] upRight(int x, int y) {
        if (y % 2 == 1)  x++; // adjust for indent on odd lines
        return new int[] {x, y - 1};
    }

    /**
     * Gets down-left cell.
     * @param x
     * @param y
     * @return coordinates.
     */
    public static int[] downLeft(int x, int y) {
        if (y % 2 == 0) x--;
        return new int[] {x, y + 1};
    }

    /**
     * Gets down-right cell.
     * @param x
     * @param y
     * @return coordinates.
     */
    public static int[] downRight(int x, int y) {
        if (y % 2 == 1) x++;
        return new int[] {x, y + 1};
    }

    /**
     * Gets right cell.
     * @param x
     * @param y
     * @return coordinates.
     */
    public static int[] right(int x, int y) {
        return new int[] {x + 1, y};
    }

    /**
     * Gets left cell.
     * @param x
     * @param y
     * @return coordinates.
     */
    public static int[] left(int x, int y) {
        return new int[] {x - 1, y};
    }

    /**
     * Gets up-left cell.
     * @return coordinates.
     */
    public int[] getUpLeft() {
        if (y % 2 == 0) x--;
        return new int[] {x, y - 1};
    }

    /**
     * Gets up-right cell.
     * @return coordinates.
     */
    public int[] getUpRight() {
        if (y % 2 == 1) x++;
        return new int[] {x, y - 1};
    }

    /**
     * Gets down-left cell.
     * @return coordinates.
     */
    public int[] getDownLeft() {
        if (y % 2 == 0) x--;
        return new int[] {x, y + 1};
    }

    /**
     * Gets down-right cell.
     * @return coordinates.
     */
    public int[] getDownRight() {
        if (y % 2 == 1) x++;
        return new int[] {x, y + 1};
    }

    /**
     * Gets right cell.
     * @return coordinates.
     */
    public int[] getRight() {
        return new int[] {x + 1, y};
    }

    /**
     * Gets left cell.
     * @return coordinates.
     */
    public int[] getLeft() {
        return new int[] {x - 1, y};
    }
}
