package World;

import Ant.Ant;

/**
 * Contains all the information stored in cells. The cells make up the grid of the world and have many types.
 * @author David
 */
public class Cell {
    /**
     * Type of the cell.
     */
    private CellType type;
    /**
     * Keeps track of checked for the parser.
     */
    private boolean checked;
    /**
     * Red ants.
     */
    private boolean redAnt;
    /**
     * Black ants.
     */
    private boolean blackAnt;
    /**
     * Ant instance.
     */
    private Ant ant;
    /**
     * If marker is red.
     */
    private boolean[] markedRed;
    /**
     * If marker is black.
     */
    private boolean[] markedBlack;
    /**
     * Amount of food.
     */
    int food;
    /**
     * Previous type.
     */
    private CellType previousType;

    /**
     * Constructor for the Cell class. Initialises all the variables.
     * @param type type of cell
     */
    public Cell(CellType type) {
        this.type = type;
        previousType= type;
        checked = false; // for the parser
        redAnt= false;
        blackAnt = false;
        markedRed = new boolean[6];
        markedBlack = new boolean[6];
        if (type==CellType.FOOD){food=5;}
        else{food=0;}
    }

    /**
     * @return type.
     */
    public CellType getType() {
        return type;
    }

    /**
     * Checks if the cell has been looked at by the parser
     * @return boolean value if checked
     */
    public boolean getChecked(){
        return checked;
    }

    /**
     * Sets the food on the previous cell.
     * @param numFood amount of food.
     */
    public void setFood(int numFood){
        if (type != CellType.FOOD) {
            previousType = type;
        }
        type = CellType.FOOD;
        food +=numFood;
    }

    /**
     * Decrements food.
     */
    public void decrementFood(){
        food--;
        if (food==0 && (previousType == CellType.BLACKANTHILL || previousType == CellType.REDANTHILL )){
            type = previousType;
        }
        else if (food ==0){
            type = CellType.CLEAR;
        }
    }

    /**
     * Sets the cell as checked when parsed
     */
    public void setChecked(){
        checked = true;
    }

    /**
     * Sets the cell as marked by a red ant.
     * @param b true or false.
     */
    public void setRedAnt(boolean b){
       redAnt = b;
    }

    /**
     * Sets the cell as marked by a black ant.
     * @param b true of false.
     */
    public void setBlackAnt(boolean b){
        blackAnt = b;
    }

    /**
     * Sets the ant as the one inputted.
     * @param ant ant.
     */
    public void setAnt(Ant ant) {this.ant = ant;}

    /**
     * @return true if contains red ant.
     */
    public boolean containsRedAnt(){
        return redAnt;
    }

    /**
     * @return true if contains black ant.
     */
    public boolean containsBlackAnt(){ return blackAnt; }

    /**
     * Sets cell with black marker i.
     * @param i marker.
     * @param b true or false.
     */
    public void setMarkedBlack(int i, boolean b){
        markedBlack[i] = b;
    }

    /**
     * Sets cell with red marker i.
     * @param i marker.
     * @param b true or false.
     */
    public void setMarkedRed(int i, boolean b){
        markedRed[i] = b;
    }

    /**
     * @return ant.
     */
    public Ant getAnt(){ return ant; }

    /**
     * @return cell marked black.
     */
    public boolean[] getMarkedBlack(){ return markedBlack;
    }

    /**
     * @return cell marked red.
     */
    public boolean[] getMarkedRed(){ return markedRed; }

    /**
     * @return previous cell type.
     */
    public CellType previousType(){
        return previousType;
    }

    /**
     * @return food.
     */
    public int getFood(){
        return  food;
    }
}





