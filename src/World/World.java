/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;


import java.io.*;

/**
 * Hold information about the world in a 2D array which stores cells
 * @author David
 */
public class World {
   private  Cell[][] grid;
   private  int sizeX;
   private int sizeY;

   private int numRocks = 0;
   private int numAnthills = 0;
   private  int numFood = 0;
    private  int numRedAnts = 0;
    private  int numBlackAnts = 0;

    /**
     * Constructor when parsed the world from the world generator
     * @param grid
     */
    public World(Cell[][] grid) {
        this.grid = grid;
        calcData();
    }

    /**
     * Constructor that reads in a .world file and init a 2D array of cell
     * @param filePath
     */
    public World(String filePath) {
        try {
            FileReader fr = new FileReader(filePath);
            BufferedReader textReader = new BufferedReader(fr);

            sizeX = Integer.parseInt(textReader.readLine());
            sizeY = Integer.parseInt(textReader.readLine());

            grid = new Cell[sizeX][sizeY];

            String textLine;
            for (int i = 0; i < sizeY; i++) {
                textLine = textReader.readLine();

                int x =0;
                for (int j = 0; j < textLine.length(); j++) {
                    char c = textLine.charAt(j);
                    switch (c) {
                        case '#':
                            grid[x][i] = new Cell(CellType.ROCK);
                            x++;
                            break;
                        case '5':
                            grid[x][i] = new Cell(CellType.FOOD);
                            x++;
                            break;
                        case '+':
                            grid[x][i] = new Cell(CellType.REDANTHILL);
                            grid[x][i].setRedAnt(true);
                            x++;
                            break;
                        case '-':
                            grid[x][i] = new Cell(CellType.BLACKANTHILL);
                            grid[x][i].setBlackAnt(true);
                            x++;
                            break;
                        case '.':
                            grid[x][i] = new Cell(CellType.CLEAR);
                            x++;
                            break;
                    }
                    //Process char
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // read in file from text;
        //create new grid of cells;
        calcData();
    }

    /**
     * For testing outputs the world to the console
     */
    public void display(){
        System.out.println(sizeX);
        System.out.println(sizeY);
        for (int i = 0; i < grid.length; i++) {
            if (i % 2 == 1) {
                System.out.print(" ");
            }
            for (int j = 0; j < grid[0].length; j++) {
                CellType t = grid[j][i].getType();

                switch (t) {
                    case CLEAR:
                        System.out.print(". ");
                        break;
                    case REDANTHILL:
                        System.out.print("+ ");
                        break;
                    case BLACKANTHILL:
                        System.out.print("- ");
                        break;
                    case FOOD:
                        System.out.print("5 ");
                        break;
                    case ROCK:
                        System.out.print("# ");
                        break;
                    default:
                        System.out.print("something went wrong");
                        break;
                }


            }

            System.out.println();

        }
    }

    /**
     * Adds ants to the world. Also checks the correct amount of each object on the map and prints it to the console for test purposes
     */
    public void calcData() {
        sizeX = grid.length;
        sizeY=grid.length;


        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                if (grid[j][i].containsBlackAnt()){
                    numBlackAnts++;

                }
                if (grid[j][i].containsRedAnt()){
                    numRedAnts++;

                }
                CellType t = grid[j][i].getType();


                switch (t) {

                    case REDANTHILL:
                    case BLACKANTHILL:
                        numAnthills++;
                        break;
                    case FOOD:
                        numFood++;
                        break;
                    case ROCK:
                        numRocks++;
                        break;

                }
            }
        }
        System.out.println(sizeX);
        System.out.println(numAnthills / 127);
        System.out.println(numFood / 25);
        System.out.println(numRocks - ((4 * sizeX) - 4));
    }

    /**
     * Writes the grid to a .world file
     * @param filename
     */
    public void outputFile(String filename){

        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename+".World"), "utf-8"));
            writer.write(sizeX+"\n");
            writer.write(sizeY+"\n");
        for (int i = 0; i < grid.length; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            if (i % 2 == 1) {
               // stringBuilder.append(" ");
                System.out.print(" ");
            }
            for (int j = 0; j < grid[0].length; j++) {
                CellType t = grid[j][i].getType();


                switch (t) {
                    case CLEAR:
                        stringBuilder.append(". ");
                       // System.out.print(". ");
                        break;
                    case REDANTHILL:
                        stringBuilder.append("+ ");
                      //  System.out.print("+ ");
                        break;
                    case BLACKANTHILL:
                        stringBuilder.append("- ");
                      //  System.out.print("- ");
                        break;
                    case FOOD:
                        stringBuilder.append("5 ");
                       // System.out.print("5 ");
                        break;
                    case ROCK:
                        stringBuilder.append("# ");
                       // System.out.print("# ");
                        break;
                    default:
                       // System.out.print("something went wrong");
                        break;
                }
            }
            String s = stringBuilder.toString();


                writer.write(s+"\n");
                           }}
            catch(IOException ex){            }
          try{writer.close();}catch(Exception ex){}
    }

    /**
     * Getter method for grid
     * @return Array of cells
     */
    public Cell[][] getGrid() {
        return grid;
    }

    /**
     * Getter for sizeX
     * @return sizeX Value
     */
    public int getSizeX(){
        return sizeX;
    }

    /**
     * Getter for sizeY
     * @return sizeY value
     */
    public int getSizeY(){
        return sizeY;
    }

    /**
     * Getter for numRocks
     * @return numRocks value
     */
    public int getNumRocks(){
        return numRocks;
    }

    /**
     * Getter for numAnthill
     * @return numAntHill value
     */
    public int getNumAnthills(){
        return numAnthills;
    }

    /**
     * Getter for numFood
     * @return numFood value
     */
    public int getNumFood() {
        return numFood;
    }

    /**
     * Getter for numReadAnts
     * @return numReadAnts value
     */
    public int getNumRedAnts(){
        return numRedAnts;
    }

    /**
     * Getter for numBlackAnts
     * @return numBlackAnts value
     */
    public int getNumBlackAnts(){
        return numBlackAnts;
    }
}



