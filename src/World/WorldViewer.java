/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import AntGame.Statistics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * This is the world viewer class. It takes a world as input and then displays
 * that world
 *
 */
public class WorldViewer extends JPanel {

    private final int HEXSIZE = 4;
    private final int noPoints = 6;
    private final double horGap = 2 * HEXSIZE * Math.sin(((2 * Math.PI)) * 1 / noPoints);
    private final double verGap = 3 * HEXSIZE * Math.cos(((2 * Math.PI)) * 1 / noPoints);
    private final double lineSpace = horGap / 2;
    private World world;
    private Hexagon[][] hexGrid;
    private int xSize;
    private int ySize;
    private JFrame jframe;

    /**
     * This is the constructor for the world viewer class. It takes a world as a
     * input and then creates an array of hexagons based on the size of the
     * world it then creates a JFrame and draws the hexagons on to that JFrame.
     *
     *
     * @param world
     */
    public WorldViewer(World world) {
        this.world = world;
        xSize = world.getSizeX();
        ySize = world.getSizeY();
        hexGrid = new Hexagon[xSize][ySize];
        createHexagons();
        jframe = new JFrame("WorldViewer");
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.setPreferredSize(new Dimension((int) Math.ceil(xSize * horGap) + 40, (int) Math.ceil(ySize * verGap) + 65));
        jframe.add(this);
        jframe.pack();
        jframe.setVisible(true);

    }

    /**
     * The Hex Point Creator. This method works by essentially using a function
     * which would create a circle and then using it to choose 6 points from
     * around that circle and then adding them to an array. the hexagons are
     * drawn pointy side up and even rows are indented so that there are no gaps
     * between the hexagons the function for choosing the points have been taken
     * from https://community.oracle.com/thread/1357893 and then has been
     * modified so that the hexagons display in the correct way and the odd
     * lines are indented.
     *
     * @param x The x Coordinate for the hexagon
     * @param y The y coordinate for the hexagon
     * @return an int[] where the even positions contain the x coordinates and
     * the odd positions contain the y coordinates
     */
    public int[] hexPointsCreator(int x, int y) {
        int[] hexPoints = new int[12];
        for (int i = 0; i < 6; i++) {
            if (y % 2 == 0) {
                hexPoints[i * 2] = (int) Math.round((HEXSIZE + (x * horGap)) - HEXSIZE * Math.sin((2) * Math.PI * i / noPoints));
            } else {
                hexPoints[i * 2] = (int) Math.round((HEXSIZE + (x * horGap) + lineSpace) - HEXSIZE * Math.sin((2) * Math.PI * i / noPoints));
            }
            hexPoints[(i * 2) + 1] = (int) Math.round(HEXSIZE * Math.cos((2) * Math.PI * i / noPoints) + (HEXSIZE + (y * verGap)));
        }
        return hexPoints;

    }

    /**
     * Create Hexagons. This creates a new hexagon for each cell in the given
     * world.
     *
     */
    public void createHexagons() {
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < ySize; j++) {
                hexGrid[i][j] = new Hexagon(hexPointsCreator(i, j));
            }
        }

    }

    /**
     * Update world refreshes the JFrame. This function refreshes the window so
     * that the world viewer is able to display all of the turns for the battle.
     *
     */
    public void updateWorld() {
        jframe.revalidate();
        jframe.repaint();
    }

    /**
     * This method paints all of the hexagons to the screen. This method paints
     * all of the hexagons to the screen and then it will fill the hexagon with
     * a colour depending on what is in the corresponding cell
     *
     * @param g Graphics this is what is used to paint the shapes to the JFrame
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < ySize; j++) {

                if (world.getGrid()[i][j].containsBlackAnt()) {
                    g.setColor(Color.BLACK);
                } else if (world.getGrid()[i][j].containsRedAnt()) {
                    g.setColor(Color.RED);
                } else if (world.getGrid()[i][j].getType().equals(CellType.FOOD)) {
                    g.setColor(Color.GREEN);
                } else if (world.getGrid()[i][j].getType().equals(CellType.ROCK)) {
                    g.setColor(Color.BLUE);
                } else if (world.getGrid()[i][j].getType().equals(CellType.CLEAR)) {
                    g.setColor(new Color(255, 253, 220));
                } else if (world.getGrid()[i][j].getType().equals(CellType.BLACKANTHILL)) {
                    g.setColor(Color.GRAY);
                } else if (world.getGrid()[i][j].getType().equals(CellType.REDANTHILL)) {
                    g.setColor(Color.MAGENTA);
                } else {
                    g.setColor(Color.CYAN);
                }

                g.fillPolygon(hexGrid[i][j].getPolygon());
                g.setColor(Color.BLACK);
                g.drawPolygon(hexGrid[i][j].getPolygon());
            }
        }
    }

    /**This method closes the JFrame window when called.
     *
     */
    public void closeWindow() {
        jframe.dispose();
    }

    /**
     * created for testing the class
     *
     * @return the hexGrid variable
     */
    public Hexagon[][] getHexGrid() {
        return hexGrid;
    }
}
