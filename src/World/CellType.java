package World;

/**
 * Enum for the types of cells.
 * @author David
 */
public enum CellType {ROCK, FOOD, REDANTHILL, BLACKANTHILL, CLEAR}