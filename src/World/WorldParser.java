package World;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Parses the world, checking for correctness.
 * @author Joel
 */
public class WorldParser {
    int dimX;
    int dimY;
    boolean status = false;
    Cell[][] world;
    String filePath;

    /**
     * Constructor for the world parser, gets the path for the .world file
     * @param filePath
     * @throws FileNotFoundException
     * @throws DimensionException
     * @throws InvalidTypeException
     * @throws PathException
     * @throws PerimeterException
     * @throws FoodException
     * @throws TournmentException
     * @throws AntHillException
     */
    public WorldParser(String filePath) throws FileNotFoundException, DimensionException, InvalidTypeException, PathException, PerimeterException, FoodException, TournmentException, AntHillException {
        this.filePath = filePath;

        if ( readFile(filePath) && checkPerimeter() && checkObjects()){
                 setStatus();
        }
    }

    /**
     * Sets status as true if it is correctly parsed
     */
    private void setStatus() {
        status = true;
    }

    /**
     * Getter for the status (used by the GUI)
     * @return status
     */
    public boolean getStatus(){
        return status;
    }

    /**
     * Reads each line of the world, check that dimensions are present and that all the chars in the matrix are valid
     * @param filePath
     * @return true or throws exceptions
     * @throws FileNotFoundException
     * @throws InvalidTypeException
     * @throws DimensionException
     */
    private boolean readFile(String filePath) throws FileNotFoundException, InvalidTypeException, DimensionException {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            int lineNo = 0;
            while ((line = br.readLine()) != null) {
                //Gets Dimensions
                if (lineNo == 0) {
                    if(!line.matches("[0-9]+")){
                        throw new DimensionException("Invalid dimension value: " + line);
                    }else{
                        dimX = new Integer(line);
                    }

                } else if (lineNo == 1) {
                    if(!line.matches("[0-9]+")){
                        throw new DimensionException("Invalid dimension value: " + line);
                    }else {
                        dimY = new Integer(line);
                        checkDimentions();
                        world = new Cell[dimX][dimY];
                    }
                    //Puts world into 2D array
                } else {
                    line = line.replaceAll("\\s+", "");
                    for (int i = 0; i < line.length(); i++) {
                        switch (line.charAt(i)) {
                            case '.':
                                world[lineNo - 2][i] = new Cell(CellType.CLEAR);
                                break;
                            case '+':
                                world[lineNo - 2][i] = new Cell(CellType.REDANTHILL);
                                break;
                            case '-':
                                world[lineNo - 2][i] = new Cell(CellType.BLACKANTHILL);
                                break;
                            case '5':
                                world[lineNo - 2][i] = new Cell(CellType.FOOD);
                                break;
                            case '#':
                                world[lineNo - 2][i] = new Cell(CellType.ROCK);
                                break;
                            default:
                                throw new InvalidTypeException("Invalid char detected");
                        }
                    }
                }
                lineNo++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Checks dimensions are valid for tournament
     * @return true or throws exceptions
     * @throws DimensionException
     */
    private boolean checkDimentions() throws DimensionException {
        if (dimX != 150 || dimY != 150) {
            throw new DimensionException("\nDimension X: " + dimX + "\nDimension Y: " + dimY + "\nDimensions should be 150x150");
        }else{
            return true;
        }
    }

    /**
     * Checks the perimeters are all of rock type
     * @return true or throws exception
     * @throws PerimeterException
     * @throws PathException
     */
    private boolean checkPerimeter() throws PerimeterException, PathException {
        for (int i = 0; i < 150; i++) {
            //Checks Top
            if (i == 0) {
                for (int j = 0; j < 150; j++) {
                    if (world[i][j].getType() != CellType.ROCK) {
                        throw new PerimeterException("Error in top perimeter");
                    } else {
                        if((j==0 || j==149) || (world[i+1][j].getType() == CellType.CLEAR || world[i+1][j].getType() == CellType.FOOD)){
                            world[i][j].setChecked();
                        }else{
                            throw new PathException("Invalid type next to perimeter");
                        }

                    }
                }
                //Checks Bottom
            } else if (i == 149) {
                for (int j = 0; j < 150; j++) {
                    if (world[i][j].getType() != CellType.ROCK) {
                        throw new PerimeterException("Error in bottom perimeter");
                    } else {
                        if((j==0 || j==149) || (world[i-1][j].getType() == CellType.CLEAR || world[i-1][j].getType() == CellType.FOOD)){
                            world[i][j].setChecked();
                        }else{
                            throw new PathException("Invalid type next to perimeter");
                        }
                    }
                }
                //Checks Left
            } else {
                if (world[i][0].getType() != CellType.ROCK || world[i][149].getType() != CellType.ROCK) {
                    throw new PerimeterException("Error in Left perimeter");
                } else {
                    if((world[i][1].getType() == CellType.CLEAR || world[i][1].getType() == CellType.FOOD) && (world[i][148].getType() == CellType.CLEAR || world[i][148].getType() == CellType.FOOD)){
                        world[i][0].setChecked();
                        world[i][149].setChecked();
                    }else{
                        throw new PathException("Invalid type next to perimeter");
                    }
                }

            }
        }
        return true;
    }

    /**
     * Checks that all objects are of the correct size and of the right amount.
     * @return true or throws exception
     * @throws AntHillException
     * @throws PathException
     * @throws FoodException
     * @throws TournmentException
     */
    private boolean checkObjects() throws AntHillException, PathException, FoodException, TournmentException {

        int redAnthill = 0;
        int blackAnthill = 0;
        int rocks = 0;
        int food = 0;

        for (int i = 0; i < dimX; i++) {
            for (int j = 0; j < dimY; j++) {
                if (!world[i][j].getChecked()) {
                    switch (world[i][j].getType()) {
                        case CLEAR:
                            world[i][j].setChecked();
                            break;
                        case REDANTHILL:
                            if(redAnthill(i, j)){
                                redAnthill++;
                            }
                            break;
                        case BLACKANTHILL:
                            if(blackAnthill(i, j)){
                                blackAnthill++;
                            }
                            break;
                        case FOOD:
                            if(checkFood(i, j)){
                                food++;
                            }
                            break;
                        case ROCK:
                            if(checkRock(i, j)){
                                rocks++;
                            }
                            break;
                    }
                }
            }
        }
        if(!(redAnthill == 1 && blackAnthill == 1 && food == 11 && rocks == 14)){
            throw new TournmentException("Invalid amount of objects: \nRed anthills: " + redAnthill + "/1\nBlack anthill: " + blackAnthill +
            "/1\nFood blobs: " + food + "/11\nRocks: " + rocks + "/14");
        }else{
            return true;
        }
    }

    /**
     * Checks the red ant hill is correctly formed
     * @param i
     * @param j
     * @return
     * @throws AntHillException
     */
    private boolean redAnthill(int i, int j) throws AntHillException {
        int line = 0;
        int width = 6;
        int col = j;
        int valid = 0;
        int parity1 = 0;
        int parity2 =1;
        int offset = 2;
        if (i % 2 == 0) {
            parity1 = 1;
            parity2 = 0;
            offset = 1;
        }
        for(int x=i; x < i+13; x++) {
            if (line < 7) {
                if (line > 0 && line % 2 == parity1) {
                    if (world[i + offset][j - 1].getType() == CellType.REDANTHILL) {
                        col = col - 1;
                    } else {
                        col = col + 1;
                    }
                }
                width++;
                for (int y = col; y < col + width; y++) {
                    if (world[x][y].getType() == CellType.REDANTHILL) {
                        valid++;
                        world[x][y].setChecked();
                    } else {
                        throw new AntHillException("Error in red anthill");
                    }
                }
                line++;
            } else {
                if (line > 0 && line % 2 == parity2) {
                    if (world[i + offset][j - 1].getType() == CellType.REDANTHILL) {
                        col = col + 1;
                    } else {
                        col = col - 1;
                    }
                }
                width--;
                for (int y = col; y < col + width; y++) {
                    if (world[x][y].getType() == CellType.REDANTHILL) {
                        valid++;
                        world[x][y].setChecked();
                    } else {
                        throw new AntHillException("Error in red anthill");
                    }
                }
                line++;
            }
        }
        if(valid != 127){
            throw new AntHillException("Error in redanthill size");
        }
        return true;
    }

    /**
     * Checks black anthill is correctly parsed
     * @param i
     * @param j
     * @return
     * @throws AntHillException
     */
    private boolean blackAnthill(int i, int j) throws AntHillException {
        int line = 0;
        int width = 6;
        int col = j;
        int valid = 0;
        int parity1 = 0;
        int parity2 =1;
        int offset = 2;
        if (i % 2 == 0) {
            parity1 = 1;
            parity2 = 0;
            offset = 1;
        }
        for(int x=i; x < i+13; x++) {
            if (line < 7) {
                if (line > 0 && line % 2 == parity1) {
                    if (world[i + offset][j - 1].getType() == CellType.BLACKANTHILL) {
                        col = col - 1;
                    } else {
                        col = col + 1;
                    }
                }
                width++;
                for (int y = col; y < col + width; y++) {
                    if (world[x][y].getType() == CellType.BLACKANTHILL) {
                        valid++;
                        world[x][y].setChecked();
                    } else {
                        throw new AntHillException("Error in black anthill");
                    }
                }
                line++;
            } else {
                if (line > 0 && line % 2 == parity2) {
                    if (world[i + offset][j - 1].getType() == CellType.BLACKANTHILL) {
                        col = col + 1;
                    } else {
                        col = col - 1;
                    }
                }
                width--;
                for (int y = col; y < col + width; y++) {
                    if (world[x][y].getType() == CellType.BLACKANTHILL) {
                        valid++;
                        world[x][y].setChecked();
                    } else {
                        throw new AntHillException("Error in black anthill");
                    }
                }
                line++;
            }
        }
        if(valid != 127){
            throw new AntHillException("Error in black anthill size");
        }
        return true;
    }

    /**
     * Checks that food is correctly formed and the right size
     * @param i
     * @param j
     * @return
     * @throws FoodException
     */
    private boolean checkFood(int i, int j) throws FoodException {
        int line=0;
        int col = j;
        int valid = 0;
        boolean left;
        if (i % 2 == 0) {
            left = world[i+1][j-1].getType() == CellType.FOOD;
            for(int x=i; x < i+5; x++){
                if(line > 0 && left && line % 2 == 1){
                    col = col-1;
                }else if(line > 0 && !left && line % 2 == 0) {
                    col = col+1;
                }

                for(int y=col; y < col+5; y++){
                    if(world[x][y].getType() == CellType.FOOD){
                        valid++;
                        world[x][y].setChecked();
                    }else{
                        throw new FoodException("Invalid food blob shape");
                    }
                }
                line++;
            }
        }else{
            left = world[i+1][j].getType() == CellType.FOOD;
            for(int x=i; x < i+5; x++){
                if(line > 0 && left && line % 2 == 0){
                    col = col-1;
                }else if(line > 0 && !left && line % 2 == 1){
                    col = col+1;
                }
                for(int y=col; y < col+5; y++){
                    if(world[x][y].getType() == CellType.FOOD){
                        valid++;
                        world[x][y].setChecked();
                    }else{
                        throw new FoodException("Invalid food blob shape");
                    }
                }
                line++;
            }
        }
        if(valid != 25){
            throw  new FoodException("Invalid food blob size");
        }
        return true;
    }

    /**
     * Checks that rock is the correct size and that the rock has nothing in the space arround it
     * @param i
     * @param j
     * @return
     * @throws PathException
     */
    private boolean checkRock(int i, int j) throws PathException {
        int valid = 0;

        if (i % 2 == 0) {
            if (world[i-1][j-1].getType() == CellType.CLEAR || world[i-1][j-1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i-1][j].getType() == CellType.CLEAR || world[i-1][j].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i][j-1].getType() == CellType.CLEAR || world[i][j-1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i][j+1].getType() == CellType.CLEAR || world[i][j+1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i+1][j-1].getType() == CellType.CLEAR || world[i+1][j-1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i+1][j].getType() == CellType.CLEAR || world[i+1][j].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
        }else{
            if (world[i-1][j+1].getType() == CellType.CLEAR || world[i-1][j+1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i-1][j].getType() == CellType.CLEAR || world[i-1][j].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i][j-1].getType() == CellType.CLEAR || world[i][j-1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i][j+1].getType() == CellType.CLEAR || world[i][j+1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i+1][j+1].getType() == CellType.CLEAR || world[i+1][j+1].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
            if (world[i+1][j].getType() == CellType.CLEAR || world[i+1][j].getType() == CellType.FOOD) {
                valid++;
            } else {
                throw new PathException("Invalid type next to rock");
            }
        }

        if(valid != 6){
            throw new PathException("Invalid rock parimiter");
        }
        return true;
    }
}

class InvalidTypeException extends Exception
{public InvalidTypeException() {} public InvalidTypeException(String message) {super(message);}}

class DimensionException extends Exception
{public DimensionException() {} public DimensionException(String message) {super(message);}}

class PerimeterException extends Exception
{public PerimeterException() {} public PerimeterException(String message) {super(message);}}

class AntHillException extends Exception
{public AntHillException() {} public AntHillException(String message) {super(message);}}

class FoodException extends Exception
{public FoodException() {} public FoodException(String message) {super(message);}}

class TournmentException extends Exception
{public TournmentException() {} public TournmentException(String message) {super(message);}}

class PathException extends Exception
{public PathException() {} public PathException(String message) {super(message);}}