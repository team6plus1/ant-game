package World;

import java.lang.System;
import java.util.Random;

/**
 * Generates a random world from given parameters.
 * @author David
 */
public class WorldGenerator {


    /**
     * Size of the map.
     */
    private int sizeMap;
    /**
     * Number of rocks.
     */
    private int numRocks;
    /**
     * Size of rocks.
     */
    private int sizeRock;
    /**
     * Number of anthills.
     */
    private int numAnthills;
    /**
     * Number of food blobs.
     */
    private int numFood;
    /**
     * Size of the anthills.
     */
    private int sizeAnthill;
    /**
     * Size of the food blobs.
     */
    private int sizeFood;
    /**
     * World.
     */
    private Cell[][] grid;

    /**
     * Constructs a WorldGenerator with the map size, number of ant hills, size of the anthill, number of food cells,
     * size of the food, number of rocks and the size of each rock.
     *
     * @param sizeMap size of map.
     * @param numAnthills number of anthills.
     * @param sizeAnthill size of anthills.
     * @param numFood number of food blobs.
     * @param sizeFood size of food blobs.
     * @param numRocks number of rocks.
     * @param sizeRock size of rocks.
     */
    public WorldGenerator(int sizeMap, int numAnthills, int sizeAnthill, int numFood, int sizeFood, int numRocks, int sizeRock) {
        this.sizeMap = sizeMap;
        this.numAnthills = numAnthills;
        this.numFood = numFood;
        this.numRocks = numRocks;
        this.sizeAnthill = sizeAnthill;
        this.sizeFood = sizeFood;
        grid = new Cell[sizeMap][sizeMap];
        this.sizeRock = sizeRock;
        generate();
    }

    /**
     * Returns the World class created from the grid.
     *
     * @return generated World class
     */
    public World getMap() {
        return new World(grid);
    }


    /**
     * Randomly generates the Cells for the World
     * <p>
     * Loops through the map to find the perimeter and sets the cells to rocky.
     * Also randomly creates ant hills in any remaining free spaces, food and rocky cells.
     * </p>
     */
    public void generate() {
        //gen perimeter
        for (int i = 0; i < sizeMap; i++) {
            grid[0][i] = new Cell(CellType.ROCK);
            grid[i][0] = new Cell(CellType.ROCK);
            grid[sizeMap - 1][i] = new Cell(CellType.ROCK);
            grid[i][sizeMap - 1] = new Cell(CellType.ROCK);
        }

        int i = 0;
        Random rand = new Random();

        while (i < numAnthills) {
            int x = rand.nextInt(sizeMap - 2) + 1;
            int y = rand.nextInt(sizeMap - 2) + 1;
            if (i % 2 == 0) {
                if (checkSpace(CellType.REDANTHILL, x, y, 0)) {
                    createAnthill(x, y, 1);
                    i++;
                }
            } else {
                if (checkSpace(CellType.BLACKANTHILL, x, y, 0)) {
                    createAnthill(x, y, 2);
                    i++;
                }
                //randomly place 2 anthills;
            }
        }

        i = 0;
        while (i < numFood) {
            int x = rand.nextInt(sizeMap - 2) + 1;
            int y = rand.nextInt(sizeMap - 2) + 1;
            int orientation = rand.nextInt(2);
            if (checkSpace(CellType.FOOD, x, y, orientation)) {
                if (orientation == 0) {
                    createFood(x, y);
                } else {
                    createFood2(x, y);
                }
                i++;
            }
        }
        i = 0;
        while (i < numRocks) {
            int x = rand.nextInt(sizeMap - 2) + 1;
            int y = rand.nextInt(sizeMap - 2) + 1;

            if (checkSpace(CellType.ROCK, x, y, 0)) {
                createRock(x, y);
                i++;
            }
        }

        fillinBlanks();
    }

    /**
     * Returns <code>true</code> if a cluster of <code>type</code> can be placed on the map starting at the co-ordinates x and y.
     *
     * @param type Cell Type (REDANTHILL|BLACKANTHILL|FOOD|ROCK)
     * @param x the x coordinate of the starting cell
     * @param y the y coordinate of the starting cell
     * @param orientation the direction Cells are position (Only relevant for Food Cells)
     * @return <code>true</code>if a cluster of <code>type</code> can be placed on the map, false otherwise.
     */
    public boolean checkSpace(CellType type, int x, int y, int orientation) {
        int[] lineStart;
        int[] position;
        switch (type) {
            //Check Space for anthill;
            case REDANTHILL:
            case BLACKANTHILL:

                lineStart = Position.upLeft(x, y);


                for (int i = 0; i < sizeAnthill + 2; i++) {
                    int loopStartFrom = 0; // first and last row loop shorter
                    if (i == 0) {
                        loopStartFrom = 1;
                    }
                    position = lineStart;
                    for (int j = loopStartFrom; j < (i + sizeAnthill + 2); j++) {
                        if (grid[position[0]][position[1]] != null) {
                            return false;
                        }
                        position = Position.right(position[0], position[1]);
                    }
                    lineStart = Position.downLeft(lineStart[0], lineStart[1]);
                }

                lineStart = Position.right(lineStart[0], lineStart[1]);

                for (int i = sizeAnthill + 2 - 1; i > 0; i--) {
                    int loopStartFrom = 0; // first and last row loop shorter

                    if (i == 1) {
                        loopStartFrom = 1;
                    }
                    position = lineStart;

                    for (int j = loopStartFrom; j < (i - 1 + sizeAnthill + 2); j++) {

                        if (grid[position[0]][position[1]] != null) {
                            return false;
                        }
                        position = Position.right(position[0], position[1]);

                    }
                    lineStart = Position.downRight(lineStart[0], lineStart[1]);
                }

                break;

            //check space  for Food
            case FOOD:
                if (orientation == 0) {
                    lineStart = Position.upLeft(x, y);


                    for (int i = 0; i < sizeFood + 2; i++) {

                        int loopStartFrom = 0; // first and last row loop shorter

                        if (i == 0 || i == sizeFood + 1) { //first and last lines need more surrounding free space
                            loopStartFrom++;
                        } else if (i == 1) {
                            lineStart = Position.left(lineStart[0], lineStart[1]);
                        }
                        position = lineStart;

                        for (int j = loopStartFrom; j < (sizeFood + 2); j++) {

                            if (grid[position[0]][position[1]] != null) {
                                return false;
                            }
                            position = Position.right(position[0], position[1]);

                        }
                        lineStart = Position.downRight(lineStart[0], lineStart[1]);

                    }
                } else if (orientation == 1) {

                    lineStart = Position.upLeft(x, y);


                    for (int i = 0; i < sizeFood + 2; i++) {

                        int loopStartFrom = 0; // first and last row loop shorter

                        if (i == 0 || i == sizeFood + 1) { //first and last lines need les surrounding free space
                            loopStartFrom++;
                        }

                        position = lineStart;

                        for (int j = loopStartFrom; j < (sizeFood + 2); j++) {

                            if (grid[position[0]][position[1]] != null) {
                                return false;
                            }
                            position = Position.right(position[0], position[1]);

                        }
                        lineStart = Position.downLeft(lineStart[0], lineStart[1]);

                    }
                } else {

                }
                break;

            //check space for ROCK
            case ROCK:
                lineStart = Position.upLeft(x, y);


                for (int i = 0; i < sizeRock + 2; i++) {

                    int loopStartFrom = 0; // first and last row loop shorter

                    if (i == 0 || i == sizeRock + 1) {
                        loopStartFrom++;
                    } else if (i == 1) {
                        lineStart = Position.left(lineStart[0], lineStart[1]);
                    }
                    position = lineStart;
                    //8 is probbly the wrong number here. 13? widest part of the heagaon?????
                    for (int j = loopStartFrom; j < (sizeRock + 2); j++) {

                        if (grid[position[0]][position[1]] != null) {
                            return false;
                        }
                        position = Position.right(position[0], position[1]);

                    }
                    lineStart = Position.downRight(lineStart[0], lineStart[1]);

                }

                break;
        }

        return true;
    }

    /**
     * Creates and stores a teams anthill on the map
     *
     * @param x the x coordinate of the starting cell
     * @param y the y coordinate of the starting cell
     * @param team the anthill's team that is being created (1 for the RedTeam, otherwise the BlackTeam)
     */
    public void createAnthill(int x, int y, int team) {

        int[] lineStart = {x, y};
        int[] position;
        CellType t;
        if (team == 1) {
            t = CellType.REDANTHILL;
        } else {
            t = CellType.BLACKANTHILL;
        }
        for (int i = 0; i < sizeAnthill; i++) {


            position = lineStart;

            for (int j = 0; j < (i + sizeAnthill); j++) {

                grid[position[0]][position[1]] = new Cell(t);
                if (team == 1) {
                    grid[position[0]][position[1]].setRedAnt(true);
                } else {
                    grid[position[0]][position[1]].setBlackAnt(true);
                }

                position = Position.right(position[0], position[1]);

            }
            lineStart = Position.downLeft(lineStart[0], lineStart[1]);
        }

        lineStart = Position.right(lineStart[0], lineStart[1]);

        for (int i = sizeAnthill - 1; i > 0; i--) {

            position = lineStart;

            for (int j = 0; j < (i - 1 + sizeAnthill); j++) {

                grid[position[0]][position[1]] = new Cell(t);
                if (team == 1) {
                    grid[position[0]][position[1]].setRedAnt(true);
                } else {
                    grid[position[0]][position[1]].setBlackAnt(true);
                }
                position = Position.right(position[0], position[1]);

            }
            lineStart = Position.downRight(lineStart[0], lineStart[1]);
        }
    }

    /**
     * Creates and stores food clusters dependent on the food size and places it diagonally facing right on the map.
     * @param x the x coordinate of the starting cell
     * @param y the y coordinate of the starting cell
     */
    public void createFood(int x, int y) {
        int[] lineStart = {x, y};
        int[] position;

        for (int i = 0; i < sizeFood; i++) {

            position = lineStart;
            for (int j = 0; j < (sizeFood); j++) {


                grid[position[0]][position[1]] = new Cell(CellType.FOOD);
                position = Position.right(position[0], position[1]);

            }

            lineStart = Position.downRight(lineStart[0], lineStart[1]);
        }

    }

    /**
     * Creates and stores food clusters dependent on the food size and places it diagonally facing left on the map.
     * @param x the x coordinate of the starting cell
     * @param y the y coordinate of the starting cell
     */
    public void createFood2(int x, int y) {
        int[] lineStart = {x, y};
        int[] position;

        for (int i = 0; i < sizeFood; i++) {

            position = lineStart;
            for (int j = 0; j < (sizeFood); j++) {


                grid[position[0]][position[1]] = new Cell(CellType.FOOD);
                position = Position.right(position[0], position[1]);

            }

            lineStart = Position.downLeft(lineStart[0], lineStart[1]);
        }

    }

    /**
     * Creates and stores rock clusters dependent on the rock size.
     * @param x the x coordinate of the starting cell
     * @param y the y coordinate of the starting cell
     */
    public void createRock(int x, int y) {

        int[] lineStart = {x, y};
        int[] position;

        for (int i = 0; i < sizeRock; i++) {

            position = lineStart;
            for (int j = 0; j < (sizeRock); j++) {


                grid[position[0]][position[1]] = new Cell(CellType.ROCK);
                position = Position.right(position[0], position[1]);

            }

            lineStart = Position.downRight(lineStart[0], lineStart[1]);
        }

    }

    /**
     * Fills the remainder of the cells as clear, any cells that haven't been marked as an Ant Hill, Food or Rocks
     */
    public void fillinBlanks() {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[j][i] == null) {
                    grid[j][i] = new Cell(CellType.CLEAR);
                }
            }
            //System.out.println();
        }
    }


    /**
     * Displays the map in console, for debugging purposes.
     */
    public void display() {
        for (int i = 0; i < grid.length; i++) {
            if (i % 2 == 1) {
                System.out.print(" ");
            }
            for (int j = 0; j < grid[0].length; j++) {
                CellType t = grid[j][i].getType();

                switch (t) {
                    case CLEAR:
                        System.out.print(". ");
                        break;
                    case REDANTHILL:
                        System.out.print("+ ");
                        break;
                    case BLACKANTHILL:
                        System.out.print("- ");
                        break;
                    case FOOD:
                        System.out.print("5 ");
                        break;
                    case ROCK:
                        System.out.print("# ");
                        break;
                    default:
                        System.out.print("something went wrong");
                        break;
                }
            }
            System.out.println();
        }
    }
}



