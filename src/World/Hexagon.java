package World;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JPanel;

/** This is the Hexagon class it stores a polygon which represents one of the hexagons on the grid.
 *
 */
public class Hexagon {

    private final Polygon hex;

    /**
     * This is the Constructor for the Hexagon class it takes a input of an
     * int[] where the even positions contain the x coordinates for the polygon
     * and the odd positions contain the y coordinates for the polygon from this
     * int[] a polygon is created.
     *
     * @param coordinates int[] where even positions are x coordinates and odd
     * positions are y coordinates
     */
    Hexagon(int[] coordinates) {
        super();
        int[] xcoords = new int[coordinates.length / 2];
        int[] ycoords = new int[coordinates.length / 2];
        for (int i = 0; i < coordinates.length; i++) {
            if (i % 2 == 0) {
                xcoords[i / 2] = coordinates[i];
            } else {
                ycoords[i / 2] = coordinates[i];
            }
        }
        this.hex = new Polygon(xcoords, ycoords, 6);
    }

    /**
     * This returns a object of type Polygon which is stored in the hex variable
     * created when the constructor is called
     *
     * @return Polygon the polygon stored in the hex variable
     */
    public Polygon getPolygon() {
        return hex;
    }
}
