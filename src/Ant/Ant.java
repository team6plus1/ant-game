package Ant;

/**
 * Class holds information for each ant in the battle this includes its ID, team, position X and Y, direction, dead or alive, state, holding food and the team brain.
 * The ant also has a method to initiate the next move in its brain stored in TeamBrain.
 * The class contains various setters and getters for variables that are needed to change.
 * @author Joel
 */
public class Ant {
    private Team team;
    private TeamBrain brain;
    private int id;
    private int posX;
    private int posY;
    private int state;
    private int direction;
    private int resting;
    private int foodDelivered;
    private boolean dead;
    private boolean hasFood;

    /**
     * Initialises the ant by setting the global variables ID, Position X and Y, Team and TeamBrain variables with the items in the parameter. And sets resting, foodDelivered, dead and has food to default.
     * @param id Ant ID
     * @param posX Ant pos x on the grid
     * @param posY Ant pos y on the grid
     * @param team Ant team
     * @param brain Ant team brain
     */
    public Ant(int id, int posX, int posY, Team team, TeamBrain brain){
        this.id =id;
        this.posX=posX;
        this.posY=posY;
        this.team = team;
        this.brain = brain;
        state = 0;
        direction = 0;
        resting = 0;
        foodDelivered = 0;
        dead = false;
        hasFood = false;
    }

    /**
     * For each turn in the battle this method is called. It checks if the ant is dead or resting if so the move will not be made otherwise a call to TeamBrain.runInstruction(State, Ant) is made that parses the state and an instance of it self.
     */
    public void makeMove(){
        if(!dead){
            if(resting == 0){
                brain.runInstruction(state, Ant.this);
            }else{
                resting--;
            }
        }
    }

    /**
     * Getter for direction
     * @return direction int value
     */
    public int getDirection() {
        return direction;
    }

    /**
     * Getter for posY
     * @return posY int value
     */
    public int getPosY() {
        return posY;
    }

    /**
     * Getter for posX
     * @return posX int value
     */
    public int getPosX() {
        return posX;
    }

    /**
     * Getter for id
     * @return id int value
     */
    public int getId() {
        return id;
    }

    /**
     * Getter for type
     * @return team TeamType enum
     */
    public Team getTeam() {
        return team;
    }

    /**
     * Getter for foodDelivered
     * @return foodDelivered int value
     */
    public int getFoodDelivered() {
        return foodDelivered;
    }
    
    /**
     * Getter for state
     * @return state int value
     */
    public int getState() {
        return state;
    }

    /**
     * Getter for dead
     * @return dead boolean value
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Getter for hasFood
     * @return hasFood boolean value
     */
    public boolean HasFood() {
        return hasFood;
    }

    /**
     * Setter for posX
     * @param posX int to set position x to 
     */
    public void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * Setter for posY
     * @param posY int to set position y to
     */
    public void setPosY(int posY) {
        this.posY = posY;
    }

    /**
     * Setter for state
     * @param state int state to set state to
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * Setter for direction
     * @param direction int value to change direction to
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * Increments resting
     * @param resting int value to increment resting by
     */
    public void setResting(int resting) {
        this.resting += resting;
    }

    /**
     * Increments foodDelivered
     * @param foodDelivered int value to increment foodDelivered by
     */
    public void setFoodDelivered(int foodDelivered) {
        this.foodDelivered += foodDelivered;
    }

    /**
     * Setter for dead
     * @param dead boolean value to set dead to 
     */
    public void setDead(boolean dead) {
        this.dead = dead;
    }

    /**
     * Setter for hasFood
     * @param hasFood boolean value to set hasfood to
     */
    public void setHasFood(boolean hasFood) {
        this.hasFood = hasFood;
    }
}