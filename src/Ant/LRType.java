package Ant;

/**
 * Enum for the turn direction.
 * @author Joel
 */
public enum LRType{LEFT, RIGHT}
