package Ant;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Parser for the ant brains.
 *
 * @author Joel
 */
public class AntBrainParser {

    /**
     * A linked list with all the collected lines to be read as instructions.
     */
    private LinkedList<String> code;

    /**
     * HashMap for the states and instructions.
     */
    private HashMap<Integer, Instruction> instructions;

    /**
     * Keeps track of line numbers.
     */
    private int lineNo;

    /**
     * Will be false if brain is incorrect.
     */
    private Boolean status;

    /**
     * Constructor for the ant brain parser, parses the brain.
     *
     * @param fileName used as name of file.
     * @throws brainException Exception for when the given ant brain cannot be
     * parsed
     * @throws FileNotFoundException Exception for when the the file cannot be
     * found
     */
    public AntBrainParser(String fileName) throws brainException, FileNotFoundException {
        code = new LinkedList<>();
        instructions = new HashMap<>();
        status = false;
        readFile(fileName);
        parser();
        setStatus();
    }

    /**
     * Gets the instructions.
     *
     * @return instructions.
     */
    public HashMap getInstructions() {
        return instructions;
    }

    /**
     * Sets the status to true.
     */
    private void setStatus() {
        status = true;
    }

    /**
     * Gets the status.
     *
     * @return status.
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * Gets each line of code from the ant file.
     *
     * @param filePath of the ant brain.
     * @throws FileNotFoundException
     */
    private void readFile(String filePath) throws FileNotFoundException {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (!(line.matches("\\s|\\n|\\t|\\r|\\b") || line.isEmpty())) {
                    if (line.contains(";")) {
                        line = line.split(";")[0];
                    }
                    code.add(line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks that each instruction is constructed correctly.
     *
     * @throws brainException
     */
    private void parser() throws brainException {
        lineNo = 0;
        for (String line : code) {
            if (line.startsWith("Sense")) {
                senseDirection(line.substring(5).trim());
            } else if (line.startsWith("Mark")) {
                mark(line.substring(4).trim());
            } else if (line.startsWith("Unmark")) {
                unmark(line.substring(6).trim());
            } else if (line.startsWith("PickUp")) {
                pickup(line.substring(6).trim());
            } else if (line.startsWith("Drop")) {
                drop(line.substring(4).trim());
            } else if (line.startsWith("Turn")) {
                turn(line.substring(4).trim());
            } else if (line.startsWith("Move")) {
                move(line.substring(4).trim());
            } else if (line.startsWith("Flip")) {
                flip(line.substring(4).trim());
            } else {
                throw new brainException("Incorrect Command: " + line + " line: " + lineNo);
            }
            lineNo++;
        }
    }

    /**
     * Parses SenseDirection.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void senseDirection(String line) throws brainException {
        String[] token;
        token = line.split("\\s");

        if (token.length != 4) {
            if ((token[3].equals("Marker") && token.length != 5)) {
                throw new brainException("Incorrect parameter count on line: " + lineNo);
            }
        }

        if (token[0].equals("Here")) {
            int st1 = checkState(token[1]);
            int st2 = checkState(token[2]);
            InstructionType cond = checkCond(token[3], token);
            instructions.put(lineNo, new Sense(SenseType.HERE, st1, st2, cond));
        } else if (token[0].matches("Ahead")) {
            int st1 = checkState(token[1]);
            int st2 = checkState(token[2]);
            InstructionType cond = checkCond(token[3], token);
            instructions.put(lineNo, new Sense(SenseType.AHEAD, st1, st2, cond));
        } else if (token[0].equals("LeftAhead")) {
            int st1 = checkState(token[1]);
            int st2 = checkState(token[2]);
            InstructionType cond = checkCond(token[3], token);
            instructions.put(lineNo, new Sense(SenseType.LEFTAHEAD, st1, st2, cond));
        } else if (token[0].equals("RightAhead")) {
            int st1 = checkState(token[1]);
            int st2 = checkState(token[2]);
            InstructionType cond = checkCond(token[3], token);
            instructions.put(lineNo, new Sense(SenseType.RIGHTAHEAD, st1, st2, cond));
        } else {
            throw new brainException("Incorrect Command: " + line + " line: " + lineNo);
        }
    }

    /**
     * Parses Mark.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void mark(String line) throws brainException {
        String[] token;
        token = line.split("\\s");
        if (token.length != 2) {
            throw new brainException("Incorrect parameter count on line: " + lineNo);
        }
        int i = checkI(token[0]);
        int st = checkState(token[1]);
        instructions.put(lineNo, new Mark(i, st));
    }

    /**
     * Parses Unmark.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void unmark(String line) throws brainException {
        String[] token;
        token = line.split("\\s");
        if (token.length != 2) {
            throw new brainException("Incorrect parameter count on line: " + lineNo);
        }
        int i = checkI(token[0]);
        int st = checkState(token[1]);
        instructions.put(lineNo, new Unmark(i, st));
    }

    /**
     * Parses Pickup.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void pickup(String line) throws brainException {
        String[] token;
        token = line.split("\\s");
        if (token.length != 2) {
            throw new brainException("Incorrect parameter count on line: " + lineNo);
        }
        int st1 = checkState(token[0]);
        int st2 = checkState(token[1]);
        instructions.put(lineNo, new PickUp(st1, st2));
    }

    /**
     * Parses Drop.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void drop(String line) throws brainException {
        String[] token;
        token = line.split("\\s");
        if (token.length != 1) {
            throw new brainException("Incorrect parameter count on line: " + lineNo);
        }
        int st = checkState(token[0]);
        instructions.put(lineNo, new Drop(st));
    }

    /**
     * Parses turn.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void turn(String line) throws brainException {
        String[] token;
        token = line.split("\\s");
        if (token.length != 2) {
            throw new brainException("Incorrect parameter count on line: " + lineNo);
        }
        LRType lr = checkLR(token[0]);
        int st = checkState(token[1]);
        instructions.put(lineNo, new Turn(lr, st));
    }

    /**
     * Parses Move.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void move(String line) throws brainException {
        String[] token;
        token = line.split("\\s");
        if (token.length != 2) {
            throw new brainException("Incorrect parameter count on line: " + lineNo);
        }
        int st1 = checkState(token[0]);
        int st2 = checkState(token[1]);
        instructions.put(lineNo, new Move(st1, st2));
    }

    /**
     * Parses Flip.
     *
     * @param line keeps track of line number.
     * @throws brainException
     */
    private void flip(String line) throws brainException {
        String[] token;
        token = line.split("\\s");
        if (token.length != 3) {
            throw new brainException("Incorrect parameter count on line: " + lineNo);
        }
        int p = checkP(token[0]);
        int st1 = checkState(token[1]);
        int st2 = checkState(token[2]);
        instructions.put(lineNo, new Flip(p, st1, st2));
    }

    /**
     * Checks if state is correct.
     *
     * @param token used as input.
     * @return true if correct, else throws errors.
     * @throws brainException
     */
    private int checkState(String token) throws brainException {
        if (token.matches("[0-9]+")) {
            int state = Integer.parseInt(token);
            if (state > code.size() || state > 10000) {
                throw new brainException("Incorrect State: " + token + " line: " + lineNo);
            } else {
                return state;
            }
        } else {
            throw new brainException("Incorrect State: " + token + " line: " + lineNo);
        }
    }

    /**
     * Checks if marker is correct.
     *
     * @param token used as input.
     * @return true if correct, else throws errors.
     * @throws brainException
     */
    private int checkI(String token) throws brainException {
        if (token.matches("[0-5]")) {
            int i = Integer.parseInt(token);
            if (i >= 0 && i <= 5) {
                return i;
            } else {
                throw new brainException("Incorrect i: " + token + " line: " + lineNo);
            }

        } else {
            throw new brainException("Incorrect i: " + token + " line: " + lineNo);
        }
    }

    /**
     * Checks if Left/Right is correct.
     *
     * @param token used as input.
     * @return true if correct, else throws errors.
     * @throws brainException
     */
    private LRType checkLR(String token) throws brainException {
        if (token.matches("Left")) {
            return LRType.LEFT;
        } else if (token.matches("Right")) {
            return LRType.RIGHT;
        } else {
            throw new brainException("Incorrect Direction: " + token + " line: " + lineNo);
        }
    }

    /**
     * Checks if p (for Flip) is correct.
     *
     * @param token used as input.
     * @return true if correct, else throws errors.
     * @throws brainException
     */
    private int checkP(String token) throws brainException {
        if (token.matches("[0-9]+")) {
            int i = Integer.parseInt(token);
            return i;
        } else {
            throw new brainException("Incorrect i: " + token + " line: " + lineNo);
        }
    }

    /**
     * Checks if condition is correct.
     *
     * @param cond the condition to check.
     * @param token used as input.
     * @return true if correct, else throws errors.
     * @throws brainException
     */
    private InstructionType checkCond(String cond, String[] token) throws brainException {
        if (cond.matches("Friend")) {
            return InstructionType.FRIEND;
        } else if (cond.matches("Foe")) {
            return InstructionType.FOE;
        } else if (cond.matches("FriendWithFood")) {
            return InstructionType.FRIENDWITHFOOD;
        } else if (cond.matches("FoeWithFood")) {
            return InstructionType.FOEWITHFOOD;
        } else if (cond.matches("Food")) {
            return InstructionType.FOOD;
        } else if (cond.matches("Rock")) {
            return InstructionType.ROCK;
        } else if (cond.matches("Marker")) {
            if (token[4].matches("0")) {
                return InstructionType.MARKER0;
            } else if (token[4].matches("1")) {
                return InstructionType.MARKER1;
            } else if (token[4].matches("2")) {
                return InstructionType.MARKER2;
            } else if (token[4].matches("3")) {
                return InstructionType.MARKER3;
            } else if (token[4].matches("4")) {
                return InstructionType.MARKER4;
            } else if (token[4].matches("5")) {
                return InstructionType.MARKER5;
            } else {
                throw new brainException("Incorrect Marker Condition: " + cond + " line: " + lineNo);
            }
        } else if (cond.matches("FoeMarker")) {
            return InstructionType.FOEMARKER;
        } else if (cond.matches("Home")) {
            return InstructionType.HOME;
        } else if (cond.matches("FoeHome")) {
            return InstructionType.FOEHOME;
        } else {
            throw new brainException("Incorrect Condition: " + cond + " line: " + lineNo);
        }
    }
}
