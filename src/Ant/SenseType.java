package Ant;

/**
 * Enum for the direction of the ant sensing.
 * @author Joel
 */
public enum SenseType{HERE, AHEAD, LEFTAHEAD, RIGHTAHEAD}