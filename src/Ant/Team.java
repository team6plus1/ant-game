package Ant;

/**
 * Enum for the red and black teams.
 * @author David
 */
public enum Team {BLACK, RED}
