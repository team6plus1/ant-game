package Ant;

/**
 * Enum for the type of instruction possible.
 * @author Joel
 */
public enum InstructionType {FRIEND, FOE, FRIENDWITHFOOD, FOEWITHFOOD, FOOD, ROCK, FOEMARKER, HOME, FOEHOME, MARKER0, MARKER1, MARKER2, MARKER3, MARKER4, MARKER5}