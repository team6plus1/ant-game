package Ant;

import World.World;
import World.Cell;
import World.CellType;

import java.util.Random;

import World.Position;

/**
 * Created by Lucas
 * and
 * Peer programmed by Dave and Joel
 */
interface Instruction {
    void execute(Ant ant, World world);
}

/**
 * Go to state st1 if cond holds in sensedir; and to state st2 otherwise.
 */
class Sense implements Instruction {
    private final SenseType direction;
    private final InstructionType cond;
    private final int st1, st2;

    public Sense(SenseType direction, int st1, int st2, InstructionType cond) {
        this.direction = direction;
        this.st1 = st1;
        this.st2 = st2;
        this.cond = cond;
    }

    /**
     * Gets the direction the ant is facing and senses what is in that Cell.
     * @param ant used to get ant position and set next state.
     * @param world used to get what is in the Cells.
     */
    @Override
    public void execute(Ant ant, World world) {
        Cell cellToSense;
        Position position = new Position(ant.getPosX(), ant.getPosY());
        int[] p = null;

        switch (direction) {
            case HERE:
                p = new int[]{ant.getPosX(), ant.getPosY()};
                break;
            case AHEAD:
                //get the cell in front of the ant
                switch (ant.getDirection()) {
                    case 0: //right
                        p = position.getRight();
                        break;
                    case 1: //down right
                        p = position.getDownRight();
                        break;
                    case 2: //down left
                        p = position.getDownLeft();
                        break;
                    case 3: //left
                        p = position.getLeft();
                        break;
                    case 4: //up left
                        p = position.getUpLeft();
                        break;
                    case 5: //up right
                        p = position.getUpRight();
                        break;
                }
                break;
            case LEFTAHEAD:
                int leftDirection;

                //get cell to the front left (same as making the ant turn and get block ahead)
                if (ant.getDirection() == 0) {
                    leftDirection = 5;
                } else { //not 0 so {1, 2, 3, 4, 5}
                    leftDirection = ant.getDirection() - 1;
                }

                //get the cell to the front-left of the ant
                switch (leftDirection) {
                    case 0: //right
                        p = position.getRight();
                        break;
                    case 1: //down right
                        p = position.getDownRight();
                        break;
                    case 2: //down left
                        p = position.getDownLeft();
                        break;
                    case 3: //left
                        p = position.getLeft();
                        break;
                    case 4: //up left
                        p = position.getUpLeft();
                        break;
                    case 5: //up right
                        p = position.getUpRight();
                        break;
                }
                break;
            case RIGHTAHEAD:
                int rightDirection;

                //get cell to the front left (same as making the ant turn and get block ahead)
                if (ant.getDirection() == 5) {
                    rightDirection = 0;
                } else { //not 0 so {1, 2, 3, 4, 5}
                    rightDirection = ant.getDirection() + 1;
                }

                //get the cell to the front-right of the ant
                switch (rightDirection) {
                    case 0: //right
                        p = position.getRight();
                        break;
                    case 1: //down right
                        p = position.getDownRight();
                        break;
                    case 2: //down left
                        p = position.getDownLeft();
                        break;
                    case 3: //left
                        p = position.getLeft();
                        break;
                    case 4: //up left
                        p = position.getUpLeft();
                        break;
                    case 5: //up right
                        p = position.getUpRight();
                        break;
                }
                break;
        }
        if (p != null) {
            cellToSense = world.getGrid()[p[0]][p[1]];
            switch (cond) {
                case FRIEND:
                    if (cellToSense.getAnt() != null && ant.getTeam() == cellToSense.getAnt().getTeam()) {
                        ant.setState(st1);
                    } else {
                        ant.setState(st2);
                    }
                    break;
                case FOE:
                    if (cellToSense.getAnt() != null && ant.getTeam() != cellToSense.getAnt().getTeam()) {
                        ant.setState(st1);
                    } else {
                        ant.setState(st2);
                    }
                    break;
                case FRIENDWITHFOOD:
                    if (cellToSense.getAnt() != null && ant.getTeam() == cellToSense.getAnt().getTeam() && ant.HasFood()) {
                        ant.setState(st1);
                    } else {
                        ant.setState(st2);
                    }
                    break;
                case FOEWITHFOOD:
                    if (cellToSense.getAnt() != null && ant.getTeam() != cellToSense.getAnt().getTeam() && ant.HasFood()) {
                        ant.setState(st1);
                    } else {
                        ant.setState(st2);
                    }
                    break;
                case FOOD:
                    if (cellToSense.getType().equals(CellType.FOOD)) {
                        ant.setState(st1);
                    } else {
                        ant.setState(st2);
                    }
                    break;
                case ROCK:
                    if (cellToSense.getType().equals(CellType.ROCK)) {
                        ant.setState(st1);
                    } else {
                        ant.setState(st2);
                    }
                    break;
                case FOEMARKER:
                    if (ant.getTeam() == Team.BLACK) {
                        ant.setState(st2);
                        for (boolean b : cellToSense.getMarkedRed()) {
                            if (b) {
                                ant.setState(st1);
                            }
                        }
                    } else {
                        ant.setState(st2);
                        for (boolean b : cellToSense.getMarkedBlack()) {
                            if (b) {
                                ant.setState(st1);
                            }
                        }
                    }
                    break;
                case HOME:
                    if ((ant.getTeam().equals(Team.BLACK) && cellToSense.getType().equals(CellType.BLACKANTHILL)) || (ant.getTeam().equals(Team.RED) && cellToSense.getType().equals(CellType.REDANTHILL))) {
                        ant.setState(st1);
                    } else if ((ant.getTeam().equals(Team.BLACK) && cellToSense.previousType().equals(CellType.BLACKANTHILL)) || (ant.getTeam().equals(Team.RED) && cellToSense.previousType().equals(CellType.REDANTHILL))) {
                        ant.setState(st1);


                    } else {
                        ant.setState(st2);
                    }
                    break;
                case FOEHOME:
                    if ((!ant.getTeam().equals(Team.BLACK) && cellToSense.getType().equals(CellType.BLACKANTHILL)) || (!ant.getTeam().equals(Team.RED) && cellToSense.getType().equals(CellType.REDANTHILL))) {

                        ant.setState(st1);
                    } else if ((!ant.getTeam().equals(Team.BLACK) && cellToSense.previousType().equals(CellType.BLACKANTHILL)) || (!ant.getTeam().equals(Team.RED) && cellToSense.previousType().equals(CellType.REDANTHILL))) {
                        ant.setState(st1);
                    } else {
                        ant.setState(st2);
                    }
                    break;
                case MARKER0:
                    if (ant.getTeam().equals(Team.BLACK)) {
                        if (cellToSense.getMarkedBlack()[0]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    } else {
                        if (cellToSense.getMarkedRed()[0]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    }
                    break;
                case MARKER1:
                    if (ant.getTeam().equals(Team.BLACK)) {
                        if (cellToSense.getMarkedBlack()[1]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    } else {
                        if (cellToSense.getMarkedRed()[1]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    }
                    break;
                case MARKER2:
                    if (ant.getTeam().equals(Team.BLACK)) {
                        if (cellToSense.getMarkedBlack()[2]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    } else {
                        if (cellToSense.getMarkedRed()[2]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    }
                    break;
                case MARKER3:
                    if (ant.getTeam().equals(Team.BLACK)) {
                        if (cellToSense.getMarkedBlack()[3]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    } else {
                        if (cellToSense.getMarkedRed()[3]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    }
                    break;
                case MARKER4:
                    if (ant.getTeam().equals(Team.BLACK)) {
                        if (cellToSense.getMarkedBlack()[4]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    } else {
                        if (cellToSense.getMarkedRed()[4]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    }
                    break;
                case MARKER5:
                    if (ant.getTeam().equals(Team.BLACK)) {
                        if (cellToSense.getMarkedBlack()[5]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    } else {
                        if (cellToSense.getMarkedRed()[5]) {
                            ant.setState(st1);
                        } else {
                            ant.setState(st2);
                        }
                    }
                    break;
            }
        } else {
            try {
                throw new Exception("Block sensed is null");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

/**
 * Set mark i in current cell and go to st.
 */
class Mark implements Instruction {
    private final int i, st;

    public Mark(int i, int st) {
        this.i = i;
        this.st = st;
    }

    /**
     * Gets the ant's colour and places down marker i.
     * @param ant used to get positions and colour.
     * @param world used to place marker.
     */
    @Override
    public void execute(Ant ant, World world) {
        ant.setState(st);
        if (ant.getTeam() == Team.BLACK) {
            world.getGrid()[ant.getPosX()][ant.getPosY()].setMarkedBlack(i, true);
        } else {
            world.getGrid()[ant.getPosX()][ant.getPosY()].setMarkedRed(i, true);
        }
    }
}

/**
 * Clear mark i in current cell and go to st.
 */
class Unmark implements Instruction {
    private final int i, st;

    public Unmark(int i, int st) {
        this.i = i;
        this.st = st;
    }

    /**
     * Gets the ant's colour and removes marker i.
     * @param ant used to get positions and colour.
     * @param world used to get and remove marker.
     */
    @Override
    public void execute(Ant ant, World world) {
        ant.setState(st);
        if (ant.getTeam() == Team.BLACK) {
            world.getGrid()[ant.getPosX()][ant.getPosY()].setMarkedBlack(i, false);
        } else {
            world.getGrid()[ant.getPosX()][ant.getPosY()].setMarkedRed(i, false);
        }
    }
}

/**
 * Pick up food from current cell and go to st1; go to st2 if there is no food in the current cell.
 */
class PickUp implements Instruction {
    private final int st1, st2;

    public PickUp(int st1, int st2) {
        this.st1 = st1;
        this.st2 = st2;
    }

    /**
     * Checks if there is food on the current cell, sets ant's hasFood to true and goes to st1; otherwise goes to st2
     * @param ant used to get position and set food variable.
     * @param world used to get cell and food.
     */
    @Override
    public void execute(Ant ant, World world) {
        //if there is food on current
        if (!ant.HasFood() && world.getGrid()[ant.getPosX()][ant.getPosY()].getType().equals(CellType.FOOD)) {
            //set food true=
            ant.setHasFood(true);
            //decrement amount of food of that place
            world.getGrid()[ant.getPosX()][ant.getPosY()].decrementFood();
            //set state st1
            ant.setState(st1);
        } else { //there is no food
            //set state st2
            ant.setState(st2);
        }
    }
}

/**
 * Drop food in current cell and go to st.
 */
class Drop implements Instruction {
    private final int st;

    public Drop(int st) {
        this.st = st;
    }

    /**
     * If the ant has food, drops it at the ant's position, else does not.
     * @param ant used to get position and set food values.
     * @param world used to check if there is food on the cell.
     */
    @Override
    public void execute(Ant ant, World world) {
        if (ant.HasFood() && world.getGrid()[ant.getPosX()][ant.getPosY()].getFood() < 9) {
            if ((ant.getTeam().equals(Team.BLACK) && world.getGrid()[ant.getPosX()][ant.getPosY()].getType().equals(CellType.BLACKANTHILL)) || (ant.getTeam().equals(Team.RED) && world.getGrid()[ant.getPosX()][ant.getPosY()].getType().equals(CellType.REDANTHILL))) {
                ant.setFoodDelivered(1);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setFood(1);
            } else if ((ant.getTeam().equals(Team.BLACK) && world.getGrid()[ant.getPosX()][ant.getPosY()].previousType().equals(CellType.BLACKANTHILL)) || (ant.getTeam().equals(Team.RED) && world.getGrid()[ant.getPosX()][ant.getPosY()].previousType().equals(CellType.REDANTHILL))) {
                ant.setFoodDelivered(1);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setFood(1);

        } else {
            world.getGrid()[ant.getPosX()][ant.getPosY()].setFood(1);
        }
        ant.setHasFood(false);
    }

    ant.setState(st);
}
}

/**
 * Turn left or right and go to st.
 */
class Turn implements Instruction {
    private final LRType lr;
    private final int st;

    public Turn(LRType lr, int st) {
        this.lr = lr;
        this.st = st;
    }

    /**
     * If instruction is left, turns left; else turns right.
     * @param ant used to get position and set direction.
     * @param world unused.
     */
    @Override
    public void execute(Ant ant, World world) {
        if (lr == LRType.LEFT) { //turn left
            if (ant.getDirection() == 0) {
                ant.setDirection(5);
            } else { //not 0 so {1, 2, 3, 4, 5}
                ant.setDirection(ant.getDirection() - 1);
            }
        } else { //turn right
            if (ant.getDirection() == 5) {
                ant.setDirection(0);
            } else {
                ant.setDirection(ant.getDirection() + 1);
            }
        }
        ant.setState(st);
    }
}

/**
 * Move forward and go to st1; go to st2 if the cell ahead is blocked.
 */
class Move implements Instruction {
    private final int st1, st2;

    public Move(int st1, int st2) {
        this.st1 = st1;
        this.st2 = st2;
    }

    /**
     * Gets the cell in front of the ant and moves the ant if the space is free.
     *
     * @param ant   used to get the ant's position.
     * @param world used to move the ant.
     */
    @Override
    public void execute(Ant ant, World world) {
        Cell cellInFront;
        Position position = new Position(ant.getPosX(), ant.getPosY());
        int[] p = new int[]{ant.getPosX(), ant.getPosY()};

        //get the cell in front of the ant
        switch (ant.getDirection()) {
            case 0: //right
                p = position.getRight();
                break;
            case 1: //down right
                p = position.getDownRight();
                break;
            case 2: //down left
                p = position.getDownLeft();
                break;
            case 3: //left
                p = position.getLeft();
                break;
            case 4: //up left
                p = position.getUpLeft();
                break;
            case 5: //up right
                p = position.getUpRight();
                break;
        }
        cellInFront = world.getGrid()[p[0]][p[1]];

        if (!cellInFront.getType().equals(CellType.ROCK) && !cellInFront.containsBlackAnt() && !cellInFront.containsRedAnt()) { //if cell is not blocked
            //call move
            if (ant.getTeam() == Team.BLACK) {
                world.getGrid()[ant.getPosX()][ant.getPosY()].setAnt(null);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setBlackAnt(false);
                ant.setPosX(p[0]);
                ant.setPosY(p[1]);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setAnt(ant);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setBlackAnt(true);
            } else {
                world.getGrid()[ant.getPosX()][ant.getPosY()].setAnt(null);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setRedAnt(false);
                ant.setPosX(p[0]);
                ant.setPosY(p[1]);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setAnt(ant);
                world.getGrid()[ant.getPosX()][ant.getPosY()].setRedAnt(true);
            }
            //set state to st1
            ant.setResting(14);
            ant.setState(st1);
        } else {
            //set state to st2
            ant.setState(st2);
        }
    }
}

/**
 * Choose a random number x from 0 to p-1; go to st1 if x=0 and st2 otherwise.
 */
class Flip implements Instruction {
    private final int p, st1, st2;

    public Flip(int p, int st1, int st2) {
        this.p = p;
        this.st1 = st1;
        this.st2 = st2;
    }

    /**
     * Gets the random int from 0 to p.
     * @param ant used to set the next state of the ant.
     * @param world not used.
     */
    @Override
    public void execute(Ant ant, World world) {
        if (new Random().nextInt(p) == 0) {
            ant.setState(st1);
            //  System.out.println("sates 1 from flip");
        } else {
            //   System.out.println("sates 2 from flip");
            ant.setState(st2);
        }
    }
}