package Ant;

import World.World;

import java.util.HashMap;
import java.util.logging.FileHandler;

/**
 * Class to run through the instructions.
 * @author Joel
 */
public class TeamBrain {

    /**
     * Contains the instruction and state.
     */
    private HashMap<Integer, Instruction> instruction;

    /**
     * The world.
     */
    private World world;

    /**
     * Constructor to create the set of instructions.
     * @param instruction ran by ants.
     */
    public TeamBrain(HashMap instruction) {
        this.instruction = instruction;
    }

    /**
     * Adds the world the ant is in to the instructions.
     * @param world used to get the cells for the instructions.
     */
    public void addWorld(World world) {
        this.world = world;
    }

    /**
     * Runs one instruction for the ant and world.
     * @param state the state to run.
     * @param ant the ant that executes this.
     */
    public void runInstruction(int state, Ant ant) {
        Instruction i = instruction.get(state);
        i.execute(ant, world);
    }


}
